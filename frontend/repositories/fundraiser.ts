import { UpsertFundraiserDto, FundraiserDto } from "@overrun/types";

export class FundraiserRepository {
  private static readonly basePath = "fundraisers";

  public static async getAll(): Promise<FundraiserDto[] | null> {
    const response = await apiFetch<FundraiserDto[]>(`${this.basePath}`);
    return response.data.value;
  }

  public static async getById(
    fundraiserId: number,
  ): Promise<FundraiserDto | null> {
    const response = await apiFetch<FundraiserDto>(
      `${this.basePath}/${fundraiserId}`,
    );
    return response.data.value;
  }

  public static async getFromEditionId(
    editionId: number,
  ): Promise<FundraiserDto | null> {
    const response = await apiFetch<FundraiserDto>(
      `${this.basePath}/edition/${editionId}`,
    );
    return response.data.value;
  }

  public static async getFromActiveEdition(): Promise<FundraiserDto | null> {
    const response = await apiFetch<FundraiserDto>(`${this.basePath}/active`);
    return response.data.value;
  }

  public static async edit(
    dto: UpsertFundraiserDto,
  ): Promise<FundraiserDto | null> {
    const response = await apiFetch<FundraiserDto>(`${this.basePath}`, {
      method: "POST",
      body: dto,
    });
    return response.data.value;
  }
}
