import {
  UpdateDisciplineRequest,
  type Discipline,
  CreateDisciplineRequest,
} from "@overrun/types";

export class DisciplineRepository {
  private static readonly basePath = "disciplines";

  public static async getDisciplines(
    editionId?: number,
  ): Promise<Discipline[] | null> {
    const query = editionId ? { editionId } : {};
    const response = await apiFetch<Discipline[]>(`${this.basePath}`, {
      query,
    });
    return response.data.value;
  }

  public static async getDisciplineById(
    id: number,
  ): Promise<Discipline | null> {
    const response = await apiFetch<Discipline>(`${this.basePath}/${id}`);
    return response.data.value;
  }

  public static async updateDiscipline(
    id: number,
    discipline: UpdateDisciplineRequest,
  ): Promise<Discipline | null> {
    const response = await apiFetch<Discipline>(`${this.basePath}/${id}`, {
      method: "PATCH",
      body: discipline,
    });
    return response.data.value;
  }

  public static async createDiscipline(
    discipline: CreateDisciplineRequest,
  ): Promise<Discipline | null> {
    const response = await apiFetch<Discipline>(`${this.basePath}`, {
      method: "POST",
      body: discipline,
    });
    return response.data.value;
  }

  public static async deleteDisciplineById(id: number): Promise<void | null> {
    const response = await apiFetch<void>(`${this.basePath}/${id}`, {
      method: "DELETE",
    });
    return response.data.value;
  }

  public static async deleteMultipleDisciplines(
    ids: number[],
  ): Promise<void | null> {
    const response = await apiFetch<void>(`${this.basePath}`, {
      method: "DELETE",
      query: { toDelete: ids },
    });
    return response.data.value;
  }
}
