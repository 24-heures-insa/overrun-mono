import { VaDtoResponse, VAStatus, type VaDto } from "@overrun/types";

export class VaRepository {
  private static readonly basePath = "va";

  public static async findVa(
    firstname: string,
    lastname: string,
  ): Promise<VaDtoResponse> {
    const response = await apiFetch<VaDtoResponse>(`${this.basePath}`, {
      query: {
        firstname,
        lastname,
      },
    });

    if (response.data.value) return response.data.value;

    if (response.data.value) return response.data.value;
    switch (response.error.value?.statusCode) {
      case 409: // Conflit
        return { status: VAStatus.ALREADY_USED };
      case 424:
        return { status: VAStatus.EDB_ERROR };
      case 404:
        return { status: VAStatus.NOT_FOUND };
      default:
        return { status: VAStatus.UNKNOWN_ERROR };
    }
  }

  public static async confirm(email: string): Promise<VaDtoResponse> {
    const response = await apiFetch<VaDtoResponse>(`${this.basePath}/confirm`, {
      method: "POST",
      body: { email },
    });

    if (response.data.value) return response.data.value;
    switch (response.error.value?.statusCode) {
      case 409: // Conflit
        return { status: VAStatus.ALREADY_USED };
      case 424:
        return { status: VAStatus.EDB_ERROR };
      case 404:
        return { status: VAStatus.NOT_FOUND };
      default:
        return { status: VAStatus.UNKNOWN_ERROR };
    }
  }

  public static async register(dto: VaDto): Promise<VaDtoResponse> {
    const response = await apiFetch<VaDtoResponse>(
      `${this.basePath}/register`,
      {
        method: "POST",
        body: dto,
      },
    );

    if (response.data.value) return response.data.value;
    switch (response.error.value?.statusCode) {
      case 409: // Conflit
        return { status: VAStatus.ALREADY_USED };
      case 424:
        return { status: VAStatus.EDB_ERROR };
      case 404:
        return { status: VAStatus.NOT_FOUND };
      default:
        return { status: VAStatus.UNKNOWN_ERROR };
    }
  }
}
