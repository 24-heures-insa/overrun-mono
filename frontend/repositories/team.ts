import {
  type Team,
  CompleteTeamDto,
  TeamCreationDto,
  TeamCreatedDto,
} from "@overrun/types";

export class TeamRepository {
  private static readonly basePath = "teams";

  public static async createTeam(
    dto: TeamCreationDto,
  ): Promise<TeamCreatedDto | null> {
    const response = await apiFetch<TeamCreatedDto>(`${this.basePath}`, {
      method: "POST",
      body: dto,
    });
    return response.data.value;
  }

  public static async getTeamsForSelection(
    editionId?: number,
  ): Promise<CompleteTeamDto[] | null> {
    const response = await apiFetch<CompleteTeamDto[]>(`${this.basePath}`, {
      method: "GET",
      params: {
        editionId,
      },
    });
    return response.data.value;
  }

  public static async getTeams(_editionId?: number): Promise<Team[] | null> {
    return [];
  }
}
