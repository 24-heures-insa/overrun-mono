import type { RaceLiteResponseDto } from "@overrun/types";

export class RaceRepository {
  private static readonly basePath = "races";

  public static async getRaces(
    editionId?: number,
  ): Promise<RaceLiteResponseDto[] | null> {
    const query = editionId ? { editionId } : {};
    const response = await apiFetch<RaceLiteResponseDto[]>(`${this.basePath}`, {
      query,
    });
    return response.data.value;
  }
}
