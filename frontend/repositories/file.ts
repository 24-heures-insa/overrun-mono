import { MyInfoResponseDto, type FileCategoryType } from "@overrun/types";

export class FileRepository {
  private static readonly basePath = "files";

  public static async getCertificateFile(): Promise<Blob | null> {
    const response = await apiFetch<Blob>(`${this.basePath}/certificate`);
    return response.data.value ?? null;
  }
  public static getCertificateFileUrl(): string {
    return apiUrl(`${this.basePath}/certificate`);
  }

  public static async uploadCertificate(
    file: FormData,
  ): Promise<MyInfoResponseDto | null> {
    const response = await apiFetch<MyInfoResponseDto>(
      `${this.basePath}/certificate`,
      { method: "POST", body: file },
    );
    return response.data.value;
  }

  public static async getFile(
    id: number,
    type: FileCategoryType,
  ): Promise<Blob | null> {
    const response = await apiFetch<Blob>(`${this.basePath}/${id}`, {
      query: { type },
    });
    return response.data.value ?? null;
  }
  public static getFileUrl(id: number, type: FileCategoryType): string {
    return apiUrl(`${this.basePath}/${id}`, { query: { type } });
  }

  public static async getImg(filename: string): Promise<Blob | null> {
    const response = await apiFetch<Blob>(
      `${this.basePath}/richEditor/${filename}`,
    );
    return response.data.value ?? null;
  }
  public static getImgUrl(filename: string): string {
    return apiUrl(`${this.basePath}/richEditor/${filename}`);
  }

  public static async uploadImage(
    image: FormData,
  ): Promise<{ url: string } | null> {
    const response = await apiFetch<{ url: string }>(
      `${this.basePath}/richEditor`,
      { method: "POST", body: image },
    );
    return response.data.value;
  }
}
