import { CreatePaymentDto, HelloassoUrlDto } from "@overrun/types";

export class PaymentRepository {
  private static readonly basePath = "payments";

  public static async createCheckoutIntent(
    dto: CreatePaymentDto,
  ): Promise<HelloassoUrlDto | null> {
    const response = await apiFetch<HelloassoUrlDto>(
      `${this.basePath}/checkout`,
      {
        method: "POST",
        body: dto,
      },
    );
    return response.data.value;
  }

  public static async getDonationAmountByEdition(
    editionId?: number,
  ): Promise<{ result: number } | null> {
    const query = editionId ? { editionId } : {};
    const response = await apiFetch<{ result: number }>(
      `${this.basePath}/donation`,
      {
        query,
      },
    );
    return response.data.value;
  }
}
