import {
  UpdateEditionRequest,
  type Edition,
  CreateEditionRequest,
} from "@overrun/types";

export class EditionRepository {
  private static readonly basePath = "editions";

  public static async activateEdition(
    editionId: number,
  ): Promise<Edition | null> {
    const response = await apiFetch<Edition>(
      `${this.basePath}/activate/${editionId}`,
      {
        method: "PATCH",
      },
    );
    return response.data.value;
  }

  public static async getActiveEdition(): Promise<Edition | null> {
    const response = await apiFetch<Edition>(`${this.basePath}/active`);
    return response.data.value;
  }

  public static async getEditions(): Promise<Edition[] | null> {
    const response = await apiFetch<Edition[]>(`${this.basePath}`);
    return response.data.value;
  }

  public static async getEditionById(id: number): Promise<Edition | null> {
    const response = await apiFetch<Edition>(`${this.basePath}/${id}`);
    return response.data.value;
  }

  public static async updateEdition(
    id: number,
    edition: UpdateEditionRequest,
  ): Promise<Edition | null> {
    const body = {
      name: edition.name,
      startDate: edition.startDate ? toISODate(edition.startDate) : undefined,
      endDate: edition.endDate ? toISODate(edition.endDate) : undefined,
      registrationStartDate: edition.registrationStartDate
        ? toISODate(edition.registrationStartDate)
        : undefined,
      registrationEndDate: edition.registrationEndDate
        ? toISODate(edition.registrationEndDate)
        : undefined,
    };
    const response = await apiFetch<Edition>(`${this.basePath}/${id}`, {
      method: "PATCH",
      body: body,
    });
    return response.data.value;
  }

  public static async createEdition(
    edition: CreateEditionRequest,
  ): Promise<Edition | null> {
    const body = {
      name: edition.name,
      startDate: toISODate(edition.startDate),
      endDate: toISODate(edition.endDate),
      registrationStartDate: toISODate(edition.registrationStartDate),
      registrationEndDate: toISODate(edition.registrationEndDate),
    };
    const response = await apiFetch<Edition>(`${this.basePath}`, {
      method: "POST",
      body: body,
    });
    return response.data.value;
  }
}
