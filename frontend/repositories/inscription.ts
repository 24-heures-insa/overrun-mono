import {
  InscriptionCancelRequestDto,
  InscriptionCreationDto,
  MyInfoResponseDto,
} from "@overrun/types";

export class InscriptionRepository {
  private static readonly basePath = "inscriptions";

  public static async register(
    dto: InscriptionCreationDto,
  ): Promise<MyInfoResponseDto | null> {
    const response = await apiFetch<MyInfoResponseDto>(
      `${this.basePath}/register`,
      {
        method: "POST",
        body: dto,
      },
    );
    return response.data.value;
  }

  public static async saveCancelRequest(
    body: InscriptionCancelRequestDto,
  ): Promise<MyInfoResponseDto | null> {
    const response = await apiFetch<MyInfoResponseDto>(
      `${this.basePath}/cancelRequest`,
      {
        method: "POST",
        body,
      },
    );
    return response.data.value;
  }
}
