import {
  type Category,
  CreateCategoryRequest,
  UpdateCategoryRequest,
} from "@overrun/types";

export class CategoryRepository {
  private static readonly basePath = "categories";

  public static async getCategories(
    editionId?: number,
  ): Promise<Category[] | null> {
    const query = editionId ? { editionId } : {};
    const response = await apiFetch<Category[]>(`${this.basePath}`, { query });
    return response.data.value;
  }

  public static async getCategoryById(id: number): Promise<Category | null> {
    const response = await apiFetch<Category>(`${this.basePath}/${id}`);
    return response.data.value;
  }

  public static async updateCategory(
    id: number,
    category: UpdateCategoryRequest,
  ): Promise<Category | null> {
    const response = await apiFetch<Category>(`${this.basePath}/${id}`, {
      method: "PATCH",
      body: category,
    });
    return response.data.value;
  }

  public static async createCategory(
    category: CreateCategoryRequest,
  ): Promise<Category | null> {
    const response = await apiFetch<Category>(`${this.basePath}`, {
      method: "POST",
      body: category,
    });
    return response.data.value;
  }

  public static async deleteCategoryById(id: number): Promise<void | null> {
    const response = await apiFetch<void>(`${this.basePath}/${id}`, {
      method: "DELETE",
    });
    return response.data.value;
  }

  public static async deleteMultipleCategories(
    ids: number[],
  ): Promise<void | null> {
    const response = await apiFetch<void>(`${this.basePath}`, {
      method: "DELETE",
      query: { toDelete: ids },
    });
    return response.data.value;
  }
}
