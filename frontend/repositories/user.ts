import type { AsyncDataRequestStatus } from "#app";
import { MyInfoResponseDto, UpdateUserInfoDTO } from "@overrun/types";

export class UserRepository {
  private static readonly basePath = "users";

  public static async getConnectedUser(): Promise<MyInfoResponseDto | null> {
    const response = await apiFetch<MyInfoResponseDto>(`${this.basePath}/me`);
    return response.data.value;
  }

  public static async updateConnectedUserInfo(
    info: UpdateUserInfoDTO,
  ): Promise<AsyncDataRequestStatus> {
    const { status } = await apiFetch<unknown>(`${this.basePath}/me`, {
      method: "PUT",
      body: info,
    });
    return status.value;
  }
}
