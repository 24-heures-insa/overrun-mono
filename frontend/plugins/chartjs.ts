import {
  Bar,
  Line,
  Doughnut,
  Pie,
  Bubble,
  PolarArea,
  Radar,
  Scatter,
} from "vue-chartjs";
import {
  Chart as ChartJS,
  Title,
  Tooltip,
  Legend,
  BarElement,
  CategoryScale,
  LinearScale,
  LineElement,
  PointElement,
  ArcElement,
} from "chart.js";

ChartJS.register(
  Title,
  Tooltip,
  Legend,
  PointElement,
  BarElement,
  CategoryScale,
  LinearScale,
  LineElement,
  ArcElement,
);

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.component("BarChart", Bar);
  nuxtApp.vueApp.component("BubbleChart", Bubble);
  nuxtApp.vueApp.component("DoughnutChart", Doughnut);
  nuxtApp.vueApp.component("LineChart", Line);
  nuxtApp.vueApp.component("PieChart", Pie);
  nuxtApp.vueApp.component("PolarAreaChart", PolarArea);
  nuxtApp.vueApp.component("RadarChart", Radar);
  nuxtApp.vueApp.component("ScatterChart", Scatter);
});
