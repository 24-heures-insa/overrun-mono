// plugins/vuetify.js
import { createVuetify } from "vuetify";
import { mdi } from "vuetify/iconsets/mdi";
import { overrunIcons } from "~/assets/overrunIcons";

export default defineNuxtPlugin((nuxtApp) => {
  const vuetify = createVuetify({
    ssr: true,
    icons: {
      defaultSet: "mdi",
      sets: {
        mdi,
        overrun: overrunIcons,
      },
    },
  });

  nuxtApp.vueApp.use(vuetify);
});
