export default defineNuxtRouteMiddleware(async () => {
  const editionStore = useEditionStore();
  if (editionStore.activatedEdition.id === undefined) {
    editionStore.fetchActiveEdition();
  }
});
