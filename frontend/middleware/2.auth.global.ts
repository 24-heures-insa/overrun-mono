import { oidcRoles } from "@overrun/types";

export default defineNuxtRouteMiddleware(async (to) => {
  // Plus d'expliation sur l'utilisation de useOidcAuth ici :
  // https://nuxtoidc.cloud/composable
  const oidc = useOidcAuth();
  await oidc.fetch();

  const notProtectedRoutes = ["/"];
  const isProtectedRoute = !notProtectedRoutes.includes(to.path);

  const isAdminPage = to.path.includes("/admin");

  if (notProtectedRoutes.includes(to.path)) return;
  if (!userIsLoggedIn(oidc) && isProtectedRoute) return navigateTo("/");
  if (!userHasRole(oidcRoles.ADMIN, oidc) && isAdminPage)
    return navigateTo("/dashboard");
});
