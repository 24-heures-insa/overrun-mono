import { oidcRoles } from "@overrun/types";

export default defineNuxtRouteMiddleware(async () => {
  const oidc = useOidcAuth();
  await oidc.fetch();

  if (!userIsLoggedIn(oidc)) return;

  if (userIsLoggedIn(oidc) && userHasRole(oidcRoles.ADMIN, oidc))
    return navigateTo("/admin");
  if (userIsLoggedIn(oidc) && userHasRole(oidcRoles.ATHLETE, oidc))
    return navigateTo("/dashboard");
});
