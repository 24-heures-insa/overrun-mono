import { oidcRoles, oidcRolesClaims } from "@overrun/types";
// Plus d'expliation sur l'utilisation de useOidcAuth ici :
// https://nuxtoidc.cloud/composable

export const userIsLoggedIn = (oidc: ReturnType<typeof useOidcAuth>) =>
  oidc.loggedIn.value;

export const userHasRole = (
  role: oidcRoles,
  oidc: ReturnType<typeof useOidcAuth>,
) => {
  // eslint-disable-next-line security/detect-object-injection
  const rolesObj = oidc.user.value?.userInfo?.[oidcRolesClaims] ?? {};
  const roles = Object.keys(rolesObj);
  return roles.indexOf(role) !== -1;
};

export async function handleLogout(oidc: ReturnType<typeof useOidcAuth>) {
  oidc.logout();
  await oidc.clear();
}
