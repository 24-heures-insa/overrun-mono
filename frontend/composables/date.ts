import { DATE_FORMAT } from "@overrun/constant";
import { type RawDateString, type RawDatetimeString } from "@overrun/types";

export function formateDateDisplay(date: string | Date) {
  return new Date(date).toLocaleDateString("FR-fr", DATE_FORMAT);
}

export function toISODate(date: string | Date): RawDatetimeString {
  const dateObj = new Date(date);
  return dateObj.toISOString() as RawDatetimeString;
}

export function htmlInputDateTime(
  date: string | Date,
  options: { hideTime?: boolean } = {},
): RawDateString | RawDatetimeString {
  const { hideTime } = options;
  const dateObj = new Date(date);
  const day = String(dateObj.getDate()).padStart(2, "0");
  const month = String(dateObj.getMonth() + 1).padStart(2, "0");
  const year = dateObj.getFullYear();
  if (!hideTime) {
    const hours = String(dateObj.getHours()).padStart(2, "0");
    const minutes = String(dateObj.getMinutes()).padStart(2, "0");
    return `${year}-${month}-${day}T${hours}:${minutes}`;
  } else {
    return `${year}-${month}-${day}`;
  }
}
