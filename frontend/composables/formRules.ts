import { VA_FORMAT } from "@overrun/constant";

import { FILE_EXTENSIONS } from "@overrun/constant";

export function required(value: string) {
  if ((value && value.length > 0) || typeof value === "number") {
    return true;
  }
  return "Champ obligatoire";
}

export function strictPositiveNumber(value: number) {
  if (value > 0) {
    return true;
  }
  return "Doit être supérieur à 0";
}

export function positiveNumber(value: number) {
  if (value >= 0) {
    return true;
  }
  return "Doit être supérieur ou égal à 0";
}

export function categorySize(
  value: number,
  monoSize: boolean,
  min: number,
  max: number,
) {
  if (monoSize || (min <= value && value <= max)) {
    return true;
  }
  return "La taille minimale doit être inférieure ou égale à la taille maximale";
}

export function isNumber(value: string) {
  if (/[0-9]+/.test(value)) {
    return true;
  }
  return "Doit être un nombre.";
}

export function isVaNumber(value: string) {
  if (VA_FORMAT.test(value)) {
    return true;
  }
  return 'Format : "c" suivi de 12 chiffres.';
}

export function checkFileExtension(filename: string) {
  const ext = filename.split(".").at(-1) ?? "";
  const allExtensions = Object.values(FILE_EXTENSIONS).flatMap(
    ({ extension }) => extension,
  );
  if (allExtensions.includes(ext)) {
    return true;
  }
  return `Formats acceptés: ${allExtensions.join(", ")}`;
}
