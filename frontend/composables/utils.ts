import {
  type CertificateStatusType,
  PaymentStatus,
  InscriptionStatus,
  type Category,
} from "@overrun/types";
import { CertificateStatus } from "@overrun/types";

export const STATUS_COLOR = {
  SUCCESS: "success",
  WARNING: "warning",
  ERROR: "error",
  INFO: "info",
};

type paramStatusColor = {
  va?: boolean;
  payment?: PaymentStatus;
  inscription?: InscriptionStatus;
  certificate?: CertificateStatusType;
};

export function statusColor(params: paramStatusColor): string {
  const { va, payment, inscription, certificate } = params;
  if (payment) {
    switch (payment) {
      case PaymentStatus.NOT_STARTED:
      case PaymentStatus.PENDING:
        return STATUS_COLOR.WARNING;
      case PaymentStatus.REFUND:
      case PaymentStatus.REFUNDING:
        return STATUS_COLOR.INFO;
      case PaymentStatus.VALIDATED:
        return STATUS_COLOR.SUCCESS;
      case PaymentStatus.REFUSED:
        return STATUS_COLOR.ERROR;
    }
  } else if (inscription) {
    switch (inscription) {
      case InscriptionStatus.PENDING:
        return STATUS_COLOR.WARNING;
      case InscriptionStatus.VALIDATED:
        return STATUS_COLOR.SUCCESS;
      case InscriptionStatus.CANCELLED:
      case InscriptionStatus.CANCEL_ASKED:
        return STATUS_COLOR.ERROR;
    }
  } else if (certificate) {
    switch (certificate) {
      case CertificateStatus.PENDING:
        return STATUS_COLOR.WARNING;
      case CertificateStatus.VALIDATED:
        return STATUS_COLOR.SUCCESS;
      case CertificateStatus.REFUSED:
        return STATUS_COLOR.ERROR;
    }
  } else if (va !== undefined) {
    return va ? STATUS_COLOR.SUCCESS : STATUS_COLOR.WARNING;
  }
  return STATUS_COLOR.ERROR;
}

export function centsToEuros(price: number | undefined) {
  return price ? price / 100 : 0;
}

export function eurosToCents(price: number) {
  return price * 100;
}

export function sizeCategory(category: Category): string {
  if (category.minTeamMembers === category.maxTeamMembers) {
    return `${category.minTeamMembers}`;
  } else {
    return `${category.minTeamMembers}-${category.maxTeamMembers}`;
  }
}

export function isSolo(min: number, max: number) {
  return min === 1 && max === 1;
}

export interface Interval {
  days: string;
  hours: string;
  min: string;
  sec: string;
}
