import type { UseFetchOptions } from "nuxt/app";

export async function apiFetch<T>(
  url: string,
  options: UseFetchOptions<T> = {},
) {
  const config = useRuntimeConfig();
  const oidc = useOidcAuth();

  const defaults: UseFetchOptions<T> = {
    baseURL: config.public.baseURL,
    key: url,
    headers: oidc.loggedIn.value
      ? { Authorization: `Bearer ${oidc.user.value?.accessToken}` }
      : {},
  };

  const params = { ...defaults, ...options };

  return useFetch(url, params);
}

export function apiUrl(
  path: string,
  options?: { query?: { [key: string]: string | number } },
): string {
  const baseUrl = useRuntimeConfig().public.baseURL;
  const query = options?.query ?? {};

  // nosemgrep
  const queryParams = Object.keys(query)
    .map((key) => key + "=" + query[key])
    .join("&");
  return baseUrl + "/" + path + (queryParams ? "?" : "") + queryParams;
}
