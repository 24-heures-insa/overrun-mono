export const useDrawer = defineStore("DrawerStore", {
  state: () => ({
    drawer: {
      open: false,
      src: null as Blob | null,
      title: "",
    },
  }),
  actions: {
    openDrawer(src: Blob | null, title?: string) {
      this.drawer.open = true;
      this.drawer.src = src;
      this.drawer.title = title ?? "";
    },
    closeDrawer() {
      this.drawer.open = false;
      this.drawer.src = null;
    },
  },
  getters: {
    getDrawerStatus: (state) => state.drawer,
  },
});
