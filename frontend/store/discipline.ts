import {
  CreateDisciplineRequest,
  UpdateDisciplineRequest,
  type Discipline,
} from "@overrun/types";

export const useDisciplineStore = defineStore("DisciplineStore", {
  state: () => ({
    disciplineList: [] as Discipline[],
    selectedDiscipline: {} as Discipline,
  }),
  actions: {
    async refreshDisciplinesFromEdition(options: {
      editionId?: number;
      forceReload?: boolean;
    }) {
      const { editionId, forceReload } = options;
      if (forceReload || this.disciplineList.length === 0) {
        const response = await DisciplineRepository.getDisciplines(editionId);
        if (response !== null) {
          this.disciplineList = response;
        }
      }
    },
    async setDisciplineById(id: number) {
      const discipline =
        this.disciplineList.find((c) => c.id === id) ??
        (await DisciplineRepository.getDisciplineById(id));
      if (discipline !== null) {
        this.selectedDiscipline = { ...discipline };
      }
    },
    async createDiscipline(toCreate: CreateDisciplineRequest) {
      const response = await DisciplineRepository.createDiscipline(toCreate);
      if (response !== null) {
        this.disciplineList.push(response);
      }
    },
    async updateDiscipline(id: number, toUpdate: UpdateDisciplineRequest) {
      const response = await DisciplineRepository.updateDiscipline(
        id,
        toUpdate,
      );
      if (response !== null) {
        this.disciplineList = this.disciplineList.map((c) =>
          c.id === id ? response : c,
        );
        if (this.selectedDiscipline.id === id) {
          this.selectedDiscipline = response;
        }
      }
    },
    async deleteSingleDiscipline(id: number) {
      await DisciplineRepository.deleteDisciplineById(id);
      //il faudrait récupérer le statut ici avant de continuer
      this.disciplineList = this.disciplineList.filter((c) => c.id !== id);
    },
    async deleteMultipleDisciplines(ids: number[]) {
      await DisciplineRepository.deleteMultipleDisciplines(ids);
      //il faudrait récupérer le statut ici avant de continuer
      this.disciplineList = this.disciplineList.filter(
        (c) => !ids.some((val) => c.id === val),
      );
    },
  },
  getters: {
    disciplines: (state) => state.disciplineList,
    discipline: (state) => state.selectedDiscipline,
  },
});
