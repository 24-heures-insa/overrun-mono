import { type Team } from "@overrun/types";

export const useTeamStore = defineStore("TeamStore", {
  state: () => ({
    teamList: [] as Team[],
  }),
  actions: {
    async refreshTeamsFromEdition(options: {
      editionId?: number;
      forceReload?: boolean;
    }) {
      const { editionId, forceReload } = options;
      if (forceReload || this.teamList.length === 0) {
        const response = await TeamRepository.getTeams(editionId);
        if (response != null) {
          this.teamList = response;
        }
      }
    },
  },
  getters: {
    teams: (state) => state.teamList,
  },
});
