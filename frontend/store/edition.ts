import {
  UpdateEditionRequest,
  CreateEditionRequest,
  CompleteEditionResponse,
} from "@overrun/types";

export const useEditionStore = defineStore("EditionStore", {
  state: () => ({
    activatedEdition: {} as CompleteEditionResponse,
    editionList: [] as CompleteEditionResponse[],
    selectedEdition: {} as CompleteEditionResponse,
    displayedEdition: {} as CompleteEditionResponse,
  }),
  actions: {
    async activateEdition(editionId: number) {
      const response = await EditionRepository.activateEdition(editionId);
      if (response !== null) {
        this.editionList = this.editionList.map((edition) => {
          return { ...edition, active: edition.id === response.id };
        });
        this.activatedEdition = response;
        if (editionId === this.selectedEdition?.id) {
          this.selectedEdition = response;
        }
      }
    },
    async fetchActiveEdition() {
      const response =
        this.editionList.find((edition) => edition.active) ??
        (await EditionRepository.getActiveEdition());
      if (response !== null) {
        this.activatedEdition = response;
      }
    },
    async fetchEditions(options?: { forceReload?: boolean }) {
      if (options?.forceReload || this.editionList.length === 0) {
        const response = await EditionRepository.getEditions();
        if (response !== null) {
          this.editionList = response;
        }
      }
    },
    async setEditionById(id: number) {
      const response =
        this.editionList.find((e) => e.id === id) ??
        (await EditionRepository.getEditionById(id));
      if (response !== null) {
        this.selectedEdition = response;
      }
    },
    async updateEdition(id: number, toUpdate: UpdateEditionRequest) {
      const response = await EditionRepository.updateEdition(id, toUpdate);
      if (response !== null) {
        this.editionList = this.editionList.map((edition) =>
          edition.id === id ? response : edition,
        );
        if (this.selectedEdition?.id === id) {
          this.selectedEdition = response;
        }
      }
    },
    async createEdition(toCreate: CreateEditionRequest) {
      const response = await EditionRepository.createEdition(toCreate);
      if (response !== null) {
        this.editionList = [response].concat(this.editionList);
      }
    },
  },
  getters: {
    activeEdition: (state) => state.activatedEdition,
    editions: (state) => state.editionList,
    edition: (state) => state.selectedEdition,
  },
});
