import {
  CreateCategoryRequest,
  type Category,
  UpdateCategoryRequest,
} from "@overrun/types";

export const useCategoryStore = defineStore("CategoryStore", {
  state: () => ({
    categoryList: [] as Category[],
    selectedCategory: {} as Category,
  }),
  actions: {
    async refreshCategoriesFromEdition(options: {
      editionId?: number;
      forceReload?: boolean;
    }) {
      const { editionId, forceReload } = options;
      if (forceReload || this.categoryList.length === 0) {
        const response = await CategoryRepository.getCategories(editionId);
        if (response !== null) {
          this.categoryList = response;
        }
      }
    },
    async setCategoryById(id: number) {
      const category =
        this.categoryList.find((c) => c.id === id) ??
        (await CategoryRepository.getCategoryById(id));
      if (category !== null) {
        this.selectedCategory = { ...category };
      }
    },
    async createCategory(toCreate: CreateCategoryRequest) {
      const response = await CategoryRepository.createCategory(toCreate);
      if (response !== null) {
        this.categoryList.push(response);
      }
    },
    async updateCategory(id: number, toUpdate: UpdateCategoryRequest) {
      const response = await CategoryRepository.updateCategory(id, toUpdate);
      if (response !== null) {
        this.categoryList = this.categoryList.map((c) =>
          c.id === id ? response : c,
        );
        if (this.selectedCategory.id === id) {
          this.selectedCategory = response;
        }
      }
    },
    async deleteSingleCategory(id: number) {
      await CategoryRepository.deleteCategoryById(id);
      //il faudrait récupérer le statut ici avant de continuer
      this.categoryList = this.categoryList.filter((c) => c.id !== id);
    },
    async deleteMultipleCategories(ids: number[]) {
      await CategoryRepository.deleteMultipleCategories(ids);
      //il faudrait récupérer le statut ici avant de continuer
      this.categoryList = this.categoryList.filter(
        (c) => !ids.some((val) => c.id === val),
      );
    },
  },
  getters: {
    categories: (state) => state.categoryList,
    category: (state) => state.selectedCategory,
  },
});
