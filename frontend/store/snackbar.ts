type TypeColor = "info" | "success" | "warning" | "error";

export const useSnackbar = defineStore("SnackbarStore", {
  state: () => ({
    snackbar: {
      color: "info" as TypeColor,
      text: "",
      open: false,
    },
  }),
  actions: {
    openSnackbar(
      color: "info" | "success" | "warning" | "error",
      text: string,
    ) {
      this.snackbar.color = color;
      this.snackbar.text = text;
      this.snackbar.open = true;
    },
  },
  getters: {
    getSnackbarStatus: (state) => state.snackbar,
  },
});
