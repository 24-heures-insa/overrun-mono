import { MyInfoResponseDto, UpdateUserInfoDTO } from "@overrun/types";

export const useUserStore = defineStore("UserStore", {
  state: () => ({
    user: {
      name: "John Doe",
      email: "john.doe@example.com",
    },
    me: {} as MyInfoResponseDto,
  }),
  actions: {
    setName(name: string) {
      this.user.name = name;
    },
    setEmail(email: string) {
      this.user.email = email;
    },
    register(dto: MyInfoResponseDto) {
      this.me = dto;
    },
    async updateConnectedUser(update: UpdateUserInfoDTO) {
      const statutReponse =
        await UserRepository.updateConnectedUserInfo(update);
      if (statutReponse === "success") {
        this.me = {
          ...this.me,
          ...update,
        };
      }
    },
    async fetchConnectedUser(options?: { forceReload?: boolean }) {
      if (options?.forceReload || !this.me.id) {
        const response = await UserRepository.getConnectedUser();
        if (response !== null) {
          this.me = response;
        }
      }
    },
    validateVaStatus(valid: boolean) {
      this.me = {
        ...this.me,
        currentInscription: {
          ...this.me.currentInscription,
          va: valid,
        },
      };
    },
  },
  getters: {
    getUser: (state) => state.user,
    getMe: (state) => state.me,
  },
});
