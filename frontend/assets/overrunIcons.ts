import { h } from "vue";
import type { IconSet, IconProps } from "vuetify";
import helloasso from "./overrunIcons/helloAsso.vue";

/* eslint-disable  @typescript-eslint/no-explicit-any */
const iconSvgNameToComponent: any = {
  helloasso,
};

const overrunIcons: IconSet = {
  component: (props: IconProps) =>
    h(props.tag, [
      h(iconSvgNameToComponent[props.icon as string], { class: "v-icon__svg" }),
    ]),
};

export { overrunIcons /* aliases */ };
