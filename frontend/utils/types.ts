import type { VDataTable, VTextField } from "vuetify/components";

export type variantType =
  | "outlined"
  | "plain"
  | "underlined"
  | "filled"
  | "solo"
  | "solo-inverted"
  | "solo-filled"
  | undefined;

export type TableHeaders = VDataTable["$props"]["headers"];
export type VTextFieldProps = VTextField["$props"];
