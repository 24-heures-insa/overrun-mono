import vuetify from "vite-plugin-vuetify";

const DOMAIN = URL.parse(process.env.BASE_URL || "http://localhost:3001/");

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ssr: false,
  runtimeConfig: {
    public: {
      baseURL: process.env.BASE_URL || "http://192.168.2.203:3001/",
      version: process.env.OVERRUN_VERSION || "no version",
    },
  },
  typescript: {
    strict: true,
    typeCheck: true,
  },
  app: {
    head: {
      title: "OverRun",
      meta: [
        { charset: "utf-8" },
        { name: "viewport", content: "width=device-width, initial-scale=1" },
      ],
    },
  },
  css: [
    "vuetify/lib/styles/main.sass",
    "@mdi/font/css/materialdesignicons.min.css",
  ],
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: "@use '~/assets/variables.scss' as *;",
          silenceDeprecations: ["legacy-js-api"],
        },
      },
    },
  },

  nitro: {
    preset: "node-server",
    storage: {
      oidc: {
        driver: "fs",
        base: "oidcstorage",
      },
    },
  },

  build: {
    transpile: ["vuetify"],
  },
  modules: [
    "@pinia/nuxt",
    "nuxt-oidc-auth",
    async (_options, nuxt) => {
      nuxt.hooks.hook("vite:extendConfig", (config) => {
        config.plugins ||= [];
        config.plugins.push(vuetify());
      });
    },
  ],
  imports: {
    dirs: ["store", "repositories"],
  },
  oidc: {
    defaultProvider: "zitadel",
    providers: {
      zitadel: {
        clientId: process.env.OIDC_CLIENT_ID,
        clientSecret: "", // Works with PKCE and Code flow, just leave empty for PKCE
        baseUrl: "https://zitadel.24heures.org", // For example https://PROJECT.REGION.zitadel.cloud
        redirectUri: `${DOMAIN?.origin}/auth/zitadel/callback`, // Replace with your domain
        audience: "", // Specify for id token validation, normally same as clientId
        logoutRedirectUri: `${DOMAIN?.origin}/`, // Needs to be registered in Zitadel portal
        scope: ["openid", "profile", "email", "offline_access"],
        nonce: false,
        responseType: "code",
        exposeAccessToken: true,
        exposeIdToken: true,
      },
    },
    middleware: {
      globalMiddlewareEnabled: false,
      customLoginPage: false,
    },
    devMode: {
      enabled: false,
    },
  },
});
