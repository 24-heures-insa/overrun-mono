import {
  Category,
  Discipline,
  DisciplineDuration,
  Edition,
  PrismaClient,
  Race,
  User,
  UserRole,
  Gender
} from "@prisma/client";
const prisma = new PrismaClient();
async function main() {
  const startDate = new Date();
  const endDate = new Date();
  const registrationEndDate = new Date();
  const registrationStartDate = new Date();

  const underageDatheOfBirth = new Date();
  underageDatheOfBirth.setFullYear(underageDatheOfBirth.getFullYear() - 10);
  underageDatheOfBirth.setHours(0, 0, 0, 0);

  registrationEndDate.setMonth(registrationEndDate.getMonth() + 1);
  registrationStartDate.setMonth(registrationStartDate.getMonth() + 2);
  startDate.setHours(14, 0, 0, 0);
  startDate.setMonth(
    registrationEndDate.getMonth(),
    registrationEndDate.getDate() + 1,
  );
  endDate.setHours(14, 0, 0, 0);
  endDate.setMonth(
    registrationEndDate.getMonth(),
    registrationEndDate.getDate() + 2,
  );

  const edition: Edition = {
    active: true,
    id: -1,
    name: "Mon Édition",
    registrationEndDate,
    endDate,
    registrationStartDate,
    startDate,
  };

  const categories: Category[] = [
    {
      id: -1,
      name: "Solo",
      description: "Une seule personne",
      editionId: edition.id,
      maxTeamMembers: 1,
      minTeamMembers: 1,
    },
    {
      id: -2,
      name: "Compétition",
      description: "Une petite équipe",
      editionId: edition.id,
      maxTeamMembers: 2,
      minTeamMembers: 4,
    },
    {
      id: -3,
      name: "Loisir",
      description: "Une grosse équipe",
      editionId: edition.id,
      maxTeamMembers: 5,
      minTeamMembers: 12,
    },
  ];

  const disciplines: Discipline[] = [
    {
      id: -1,
      name: "Vélo",
      description: "Les petites pédales",
      editionId: edition.id,
    },
    {
      id: -2,
      name: "Course à pied",
      description: "Les petits pédestres",
      editionId: edition.id,
    },
    {
      id: -3,
      name: "Natation",
      description: "Les petits poissons",
      editionId: edition.id,
    },
  ];

  const races: Race[] = [
    {
      id: -1,
      categoryId: -1,
      maxTeams: 100,
      name: "Vélo Solo",
      registrationPrice: 1200,
      vaRegistrationPrice: 1000,
    },
    {
      id: -2,
      categoryId: -1,
      maxTeams: 100,
      name: "Course à pied Solo",
      registrationPrice: 1000,
      vaRegistrationPrice: 800,
    },
    {
      id: -3,
      categoryId: -1,
      maxTeams: 4,
      name: "Triathlon Solo",
      registrationPrice: 2000,
      vaRegistrationPrice: 1800,
    },
    {
      id: -4,
      categoryId: -2,
      maxTeams: 100,
      name: "Vélo Compétition",
      registrationPrice: 1500,
      vaRegistrationPrice: 1400,
    },
    {
      id: -5,
      categoryId: -2,
      maxTeams: 100,
      name: "Course à pied Compétition",
      registrationPrice: 1100,
      vaRegistrationPrice: 900,
    },
    {
      id: -6,
      categoryId: -2,
      maxTeams: 4,
      name: "Triathlon Compétition",
      registrationPrice: 2500,
      vaRegistrationPrice: 2000,
    },
    {
      id: -7,
      categoryId: -3,
      maxTeams: 100,
      name: "Vélo Loisir",
      registrationPrice: 1500,
      vaRegistrationPrice: 1400,
    },
    {
      id: -8,
      categoryId: -3,
      maxTeams: 100,
      name: "Course à pied Loisir",
      registrationPrice: 1100,
      vaRegistrationPrice: 900,
    },
    {
      id: -9,
      categoryId: -3,
      maxTeams: 4,
      name: "Triathlon Loisir",
      registrationPrice: 2500,
      vaRegistrationPrice: 2000,
    },
  ];

  const disciplineDurations: DisciplineDuration[] = [
    {
      id: -1,
      raceId: -1,
      disciplineId: -1,
      duration: 24,
    },
    {
      id: -2,
      raceId: -2,
      disciplineId: -2,
      duration: 24,
    },
    {
      id: -3,
      raceId: -3,
      disciplineId: -1,
      duration: 12,
    },
    {
      id: -4,
      raceId: -3,
      disciplineId: -2,
      duration: 8,
    },
    {
      id: -5,
      raceId: -3,
      disciplineId: -3,
      duration: 4,
    },
    {
      id: -6,
      raceId: -4,
      disciplineId: -1,
      duration: 24,
    },
    {
      id: -7,
      raceId: -5,
      disciplineId: -2,
      duration: 24,
    },
    {
      id: -8,
      raceId: -6,
      disciplineId: -1,
      duration: 12,
    },
    {
      id: -9,
      raceId: -6,
      disciplineId: -2,
      duration: 8,
    },
    {
      id: -10,
      raceId: -6,
      disciplineId: -3,
      duration: 4,
    },
    {
      id: -11,
      raceId: -7,
      disciplineId: -1,
      duration: 24,
    },
    {
      id: -12,
      raceId: -8,
      disciplineId: -2,
      duration: 24,
    },
    {
      id: -13,
      raceId: -9,
      disciplineId: -1,
      duration: 12,
    },
    {
      id: -14,
      raceId: -9,
      disciplineId: -2,
      duration: 8,
    },
    {
      id: -15,
      raceId: -9,
      disciplineId: -3,
      duration: 4,
    },
  ];

  const users: User[] = [
    {
      id: -1,
      email: "user1@phi.com",
      username: "User1",
      password: "",
      role: UserRole.ADMIN,
      firstName: "Jessica",
      lastName: "Membert",
      address: "4 rue du chemin",
      zipCode: "69100",
      city: "Villeurbanne",
      country: "France",
      phoneNumber: "0707070707",
      gender: Gender.FEMALE,
      dateOfBirth: new Date("2024-01-16"),
      resetPasswordToken: "",
    },
    {
      id: -2,
      email: "user2@phi.com",
      username: "User2",
      password: "",
      role: UserRole.ATHLETE,
      firstName: "Tom",
      lastName: "D'Ébauges",
      address: "7 chemin de la rue",
      zipCode: "69100",
      city: "Villeurbanne",
      country: "France",
      phoneNumber: "0606060606",
      gender: Gender.MALE,
      dateOfBirth: underageDatheOfBirth,
      resetPasswordToken: "",
    },
  ];

  const upsertEdition = await prisma.edition.upsert({
    where: { id: -1 },
    update: {},
    create: edition,
  });
  console.log("Edition", upsertEdition.id, "upserted");

  const upsertCategories = [];
  for(const cat of categories) {
    const c = await prisma.category.upsert({
      where: { id: cat.id },
      update: {},
      create: cat,
    });
    upsertCategories.push(c.id);
  };
  console.log("Categories", upsertCategories.join(","), "upserted");

  const upsertDisciplines = [];
  for(const dis of disciplines) {
    const d = await prisma.discipline.upsert({
      where: { id: dis.id },
      update: {},
      create: dis,
    });
    upsertDisciplines.push(d.id);
  };
  console.log("Disciplines", upsertDisciplines.join(","), "upserted");

  const upsertRaces = [];
  for(const race of races) {
    const r = await prisma.race.upsert({
      where: { id: race.id },
      update: {},
      create: race,
    });
    upsertRaces.push(r.id);
  };
  console.log("Races", upsertRaces.join(","), "upserted");

  const upsertDisciplineDurations = [];
  for(const disDur of disciplineDurations) {
    const dd = await prisma.disciplineDuration.upsert({
      where: { id: disDur.id },
      update: {},
      create: disDur,
    });
    upsertDisciplineDurations.push(dd.id);
  };
  console.log(
    "Disciplines durations",
    upsertDisciplineDurations.join(","),
    "upserted",
  );

  const upsertUsers = [];
  for(const user of users) {
    const u = await prisma.user.upsert({
      where: { id: user.id },
      update: {},
      create: user,
    });
    upsertUsers.push(u.id);
  };
  console.log("Users", upsertUsers.join(","), "upserted");
}
main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
