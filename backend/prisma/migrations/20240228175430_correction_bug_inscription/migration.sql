/*
  Warnings:

  - You are about to drop the column `paymentId` on the `Inscription` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Inscription" DROP COLUMN "paymentId";
