/*
  Warnings:

  - A unique constraint covering the columns `[vaEmail,inscriptionId]` on the table `VA` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `vaEmail` to the `VA` table without a default value. This is not possible if the table is not empty.

*/
-- DropIndex
DROP INDEX "VA_va_inscriptionId_key";

-- AlterTable
ALTER TABLE "VA" ADD COLUMN     "vaEmail" TEXT NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "VA_vaEmail_inscriptionId_key" ON "VA"("vaEmail", "inscriptionId");
