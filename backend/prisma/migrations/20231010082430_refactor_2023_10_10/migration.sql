/*
  Warnings:

  - You are about to drop the column `expirateAt` on the `AdminInvite` table. All the data in the column will be lost.
  - You are about to drop the column `userId` on the `AdminInvite` table. All the data in the column will be lost.
  - You are about to drop the column `editionId` on the `Inscription` table. All the data in the column will be lost.
  - You are about to drop the column `editionId` on the `Race` table. All the data in the column will be lost.
  - You are about to drop the column `maxParticipants` on the `Race` table. All the data in the column will be lost.
  - You are about to drop the column `editionId` on the `Team` table. All the data in the column will be lost.
  - You are about to drop the `Admin` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Athlete` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `BlackListedRefreshToken` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `PasswordInvite` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `RaceDiscipline` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `RefreshToken` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `TeamAdmin` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[va,inscriptionId]` on the table `VA` will be added. If there are existing duplicate values, this will fail.
  - Changed the type of `status` on the `Certificate` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Added the required column `paymentId` to the `Inscription` table without a default value. This is not possible if the table is not empty.
  - Added the required column `address` to the `User` table without a default value. This is not possible if the table is not empty.
  - Added the required column `city` to the `User` table without a default value. This is not possible if the table is not empty.
  - Added the required column `dateOfBirth` to the `User` table without a default value. This is not possible if the table is not empty.
  - Added the required column `firstName` to the `User` table without a default value. This is not possible if the table is not empty.
  - Added the required column `gender` to the `User` table without a default value. This is not possible if the table is not empty.
  - Added the required column `lastName` to the `User` table without a default value. This is not possible if the table is not empty.
  - Added the required column `phoneNumber` to the `User` table without a default value. This is not possible if the table is not empty.
  - Added the required column `resetPasswordToken` to the `User` table without a default value. This is not possible if the table is not empty.
  - Added the required column `role` to the `User` table without a default value. This is not possible if the table is not empty.
  - Added the required column `zipCode` to the `User` table without a default value. This is not possible if the table is not empty.
  - Added the required column `validated` to the `VA` table without a default value. This is not possible if the table is not empty.

*/
-- CreateEnum
CREATE TYPE "CertificateStatus" AS ENUM ('VALIDATED', 'PENDING', 'REFUSED');

-- CreateEnum
CREATE TYPE "UserRole" AS ENUM ('ATHLETE', 'TREASURER', 'ADMIN', 'OLD_ADMIN');

-- CreateEnum
CREATE TYPE "Gender" AS ENUM ('MALE', 'FEMALE', 'OTHER');

-- DropForeignKey
ALTER TABLE "Admin" DROP CONSTRAINT "Admin_userId_fkey";

-- DropForeignKey
ALTER TABLE "AdminInvite" DROP CONSTRAINT "AdminInvite_userId_fkey";

-- DropForeignKey
ALTER TABLE "Athlete" DROP CONSTRAINT "Athlete_userId_fkey";

-- DropForeignKey
ALTER TABLE "Category" DROP CONSTRAINT "Category_editionId_fkey";

-- DropForeignKey
ALTER TABLE "Certificate" DROP CONSTRAINT "Certificate_statusUpdatedById_fkey";

-- DropForeignKey
ALTER TABLE "Discipline" DROP CONSTRAINT "Discipline_editionId_fkey";

-- DropForeignKey
ALTER TABLE "Inscription" DROP CONSTRAINT "Inscription_athleteId_fkey";

-- DropForeignKey
ALTER TABLE "Inscription" DROP CONSTRAINT "Inscription_editionId_fkey";

-- DropForeignKey
ALTER TABLE "PasswordInvite" DROP CONSTRAINT "PasswordInvite_userId_fkey";

-- DropForeignKey
ALTER TABLE "Race" DROP CONSTRAINT "Race_editionId_fkey";

-- DropForeignKey
ALTER TABLE "RaceDiscipline" DROP CONSTRAINT "RaceDiscipline_disciplineId_fkey";

-- DropForeignKey
ALTER TABLE "RaceDiscipline" DROP CONSTRAINT "RaceDiscipline_raceId_fkey";

-- DropForeignKey
ALTER TABLE "Team" DROP CONSTRAINT "Team_editionId_fkey";

-- DropForeignKey
ALTER TABLE "TeamAdmin" DROP CONSTRAINT "TeamAdmin_adminInscriptionId_fkey";

-- DropForeignKey
ALTER TABLE "TeamAdmin" DROP CONSTRAINT "TeamAdmin_teamId_fkey";

-- DropIndex
DROP INDEX "AdminInvite_userId_key";

-- DropIndex
DROP INDEX "VA_va_key";

-- AlterTable
ALTER TABLE "AdminInvite" DROP COLUMN "expirateAt",
DROP COLUMN "userId";

-- AlterTable
ALTER TABLE "Certificate" DROP COLUMN "status",
ADD COLUMN     "status" "CertificateStatus" NOT NULL;

-- AlterTable
ALTER TABLE "Discipline" ALTER COLUMN "editionId" DROP NOT NULL;

-- AlterTable
ALTER TABLE "Inscription" DROP COLUMN "editionId",
ADD COLUMN     "paymentId" INTEGER NOT NULL,
ADD COLUMN     "racingbib" INTEGER NOT NULL DEFAULT 0,
ADD COLUMN     "teamAdminId" INTEGER;

-- AlterTable
ALTER TABLE "Payment" ADD COLUMN     "refundAsked" BOOLEAN NOT NULL DEFAULT false;

-- AlterTable
ALTER TABLE "Race" DROP COLUMN "editionId",
DROP COLUMN "maxParticipants";

-- AlterTable
ALTER TABLE "Team" DROP COLUMN "editionId";

-- AlterTable
ALTER TABLE "User" ADD COLUMN     "address" VARCHAR(100) NOT NULL,
ADD COLUMN     "city" VARCHAR(50) NOT NULL,
ADD COLUMN     "country" TEXT NOT NULL DEFAULT 'France',
ADD COLUMN     "dateOfBirth" TIMESTAMP(3) NOT NULL,
ADD COLUMN     "firstName" VARCHAR(50) NOT NULL,
ADD COLUMN     "gender" "Gender" NOT NULL,
ADD COLUMN     "lastName" VARCHAR(50) NOT NULL,
ADD COLUMN     "phoneNumber" VARCHAR(12) NOT NULL,
ADD COLUMN     "resetPasswordToken" TEXT NOT NULL,
ADD COLUMN     "role" "UserRole" NOT NULL,
ADD COLUMN     "zipCode" VARCHAR(50) NOT NULL;

-- AlterTable
ALTER TABLE "VA" ADD COLUMN     "validated" BOOLEAN NOT NULL;

-- DropTable
DROP TABLE "Admin";

-- DropTable
DROP TABLE "Athlete";

-- DropTable
DROP TABLE "BlackListedRefreshToken";

-- DropTable
DROP TABLE "PasswordInvite";

-- DropTable
DROP TABLE "RaceDiscipline";

-- DropTable
DROP TABLE "RefreshToken";

-- DropTable
DROP TABLE "TeamAdmin";

-- CreateTable
CREATE TABLE "Fundraiser" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(50) NOT NULL,
    "description" TEXT NOT NULL,
    "editionId" INTEGER NOT NULL,

    CONSTRAINT "Fundraiser_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "DisciplineDuration" (
    "id" SERIAL NOT NULL,
    "duration" INTEGER NOT NULL,
    "raceId" INTEGER NOT NULL,
    "disciplineId" INTEGER NOT NULL,

    CONSTRAINT "DisciplineDuration_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Comment" (
    "id" SERIAL NOT NULL,
    "comment" TEXT NOT NULL,
    "date" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "inscriptionId" INTEGER NOT NULL,
    "authorId" INTEGER NOT NULL,

    CONSTRAINT "Comment_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Fundraiser_name_key" ON "Fundraiser"("name");

-- CreateIndex
CREATE UNIQUE INDEX "Fundraiser_editionId_key" ON "Fundraiser"("editionId");

-- CreateIndex
CREATE UNIQUE INDEX "VA_va_inscriptionId_key" ON "VA"("va", "inscriptionId");

-- AddForeignKey
ALTER TABLE "Fundraiser" ADD CONSTRAINT "Fundraiser_editionId_fkey" FOREIGN KEY ("editionId") REFERENCES "Edition"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Discipline" ADD CONSTRAINT "Discipline_editionId_fkey" FOREIGN KEY ("editionId") REFERENCES "Edition"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Category" ADD CONSTRAINT "Category_editionId_fkey" FOREIGN KEY ("editionId") REFERENCES "Edition"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "DisciplineDuration" ADD CONSTRAINT "DisciplineDuration_raceId_fkey" FOREIGN KEY ("raceId") REFERENCES "Race"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "DisciplineDuration" ADD CONSTRAINT "DisciplineDuration_disciplineId_fkey" FOREIGN KEY ("disciplineId") REFERENCES "Discipline"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Inscription" ADD CONSTRAINT "Inscription_teamAdminId_fkey" FOREIGN KEY ("teamAdminId") REFERENCES "Team"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Inscription" ADD CONSTRAINT "Inscription_athleteId_fkey" FOREIGN KEY ("athleteId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Comment" ADD CONSTRAINT "Comment_inscriptionId_fkey" FOREIGN KEY ("inscriptionId") REFERENCES "Inscription"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Comment" ADD CONSTRAINT "Comment_authorId_fkey" FOREIGN KEY ("authorId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Certificate" ADD CONSTRAINT "Certificate_statusUpdatedById_fkey" FOREIGN KEY ("statusUpdatedById") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;
