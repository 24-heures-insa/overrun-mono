// This is your Prisma schema file,
// learn more about it in the docs: https://pris.ly/d/prisma-schema
// Schéma du model https://drive.google.com/file/d/1Hrn7bhwpfVRsKXJYHYAATRO13GbHW3MN/view?usp=drive_link

generator client {
    provider      = "prisma-client-js"
    binaryTargets = ["native", "linux-musl", "darwin-arm64"]
}

datasource db {
    provider = "postgresql"
    url      = env("DATABASE_URL")
}

enum InscriptionStatus {
    PENDING
    VALIDATED
    CANCELLED
    CANCEL_ASKED
}

enum PaymentStatus {
    NOT_STARTED
    PENDING
    VALIDATED
    REFUSED
    REFUND
    REFUNDING
}

enum CertificateStatus {
    VALIDATED
    PENDING
    REFUSED
}

enum UserRole {
    ATHLETE
    TREASURER
    ADMIN
    OLD_ADMIN
}

enum Gender {
    MALE
    FEMALE
    OTHER
}

model Edition {
    id                    Int          @id @default(autoincrement())
    name                  String       @unique @db.VarChar(50)
    startDate             DateTime
    endDate               DateTime
    registrationStartDate DateTime
    registrationEndDate   DateTime
    active                Boolean
    disciplines           Discipline[]
    categories            Category[]
    fundraiser            Fundraiser?
}

model Fundraiser {
    id          Int      @id @default(autoincrement())
    name        String   @db.VarChar(50)
    description String
    edition     Edition  @relation(fields: [editionId], references: [id])
    editionId   Int      @unique
    images      String[]
}

model Discipline {
    id           Int                  @id @default(autoincrement())
    name         String               @db.VarChar(50)
    description  String               @db.Text
    raceDuration DisciplineDuration[]
    Edition      Edition?             @relation(fields: [editionId], references: [id])
    editionId    Int?
}

model Category {
    id             Int     @id @default(autoincrement())
    name           String  @db.VarChar(50)
    description    String  @db.Text
    maxTeamMembers Int     @default(1)
    minTeamMembers Int     @default(1)
    races          Race[]
    edition        Edition @relation(fields: [editionId], references: [id])
    editionId      Int
}

model Race {
    id                  Int                  @id @default(autoincrement())
    name                String               @unique
    registrationPrice   Float
    vaRegistrationPrice Float
    maxTeams            Int
    disciplineDurations DisciplineDuration[]
    category            Category             @relation(fields: [categoryId], references: [id], onDelete: Cascade)
    categoryId          Int
    inscriptions        Inscription[]
    teams               Team[]
}

model DisciplineDuration {
    id           Int        @id @default(autoincrement())
    duration     Int
    race         Race       @relation(fields: [raceId], references: [id])
    raceId       Int
    discipline   Discipline @relation(fields: [disciplineId], references: [id])
    disciplineId Int
}

// TODO renommer password en token
model Team {
    id       Int           @id @default(autoincrement())
    name     String        @unique @db.VarChar(50)
    password String        @db.VarChar(255)
    members  Inscription[] @relation(name: "teamMembers")
    race     Race          @relation(fields: [raceId], references: [id])
    raceId   Int
    admins   Inscription[] @relation(name: "teamAdmins")
}

model Inscription {
    id          Int               @id @default(autoincrement())
    status      InscriptionStatus @default(PENDING)
    createdAt   DateTime          @default(now())
    racingbib   Int               @default(0)
    team        Team?             @relation(fields: [teamId], references: [id], onDelete: SetNull, name: "teamMembers")
    teamId      Int?
    teamAdmin   Team?             @relation(name: "teamAdmins", fields: [teamAdminId], references: [id])
    teamAdminId Int?
    race        Race              @relation(fields: [raceId], references: [id])
    raceId      Int
    comment     Comment[]
    certificate Certificate?
    va          VA?
    payment     Payment?
    athlete     User              @relation(fields: [athleteId], references: [id])
    athleteId   Int
}

model Comment {
    id            Int         @id @default(autoincrement())
    comment       String
    date          DateTime    @default(now())
    inscription   Inscription @relation(fields: [inscriptionId], references: [id])
    inscriptionId Int
    author        User        @relation(fields: [authorId], references: [id])
    authorId      Int
}

model VA {
    id            Int         @id @default(autoincrement())
    va            String      @db.VarChar(1000)
    inscription   Inscription @relation(fields: [inscriptionId], references: [id])
    inscriptionId Int         @unique
    validated     Boolean
    vaEmail       String

    @@unique([vaEmail, inscriptionId])
}

model Certificate {
    id                Int               @id @default(autoincrement())
    filename          String            @db.VarChar(255)
    inscription       Inscription       @relation(fields: [inscriptionId], references: [id], onDelete: Cascade)
    inscriptionId     Int               @unique
    uploadedAt        DateTime          @default(now())
    status            CertificateStatus
    statusUpdatedAt   DateTime?         @default(now())
    statusUpdatedBy   User?             @relation(fields: [statusUpdatedById], references: [id], onDelete: SetNull)
    statusUpdatedById Int?
}

model Payment {
    id                         Int           @id @default(autoincrement())
    inscription                Inscription   @relation(fields: [inscriptionId], references: [id])
    inscriptionId              Int           @unique
    date                       DateTime      @default(now())
    raceAmount                 Int
    donationAmount             Int           @default(0)
    totalAmount                Int
    status                     PaymentStatus @default(PENDING)
    helloassoCheckoutIntentId  Int?          @unique
    helloassoCheckoutIntentUrl String?       @db.VarChar(500)
    helloassoCheckoutExpiresAt DateTime?
    helloassoPaymentReceiptUrl String?       @db.VarChar(500)
    refundAsked                Boolean       @default(false)
}

model User {
    id                 Int           @id @default(autoincrement())
    email              String        @unique @db.VarChar(50)
    username           String        @unique @db.VarChar(50)
    password           String        @db.VarChar(255)
    role               UserRole
    certificates       Certificate[]
    comments           Comment[]
    inscriptions       Inscription[]
    firstName          String        @db.VarChar(50)
    lastName           String        @db.VarChar(50)
    address            String        @db.VarChar(100)
    zipCode            String        @db.VarChar(50)
    city               String        @db.VarChar(50)
    country            String        @default("France")
    phoneNumber        String        @db.VarChar(12)
    gender             Gender
    dateOfBirth        DateTime
    resetPasswordToken String
}
