import { IsOptional, IsString, Matches } from "class-validator";
import { VA_FORMAT } from "@overrun/constant";
import { VaDto as VA } from "@overrun/types";

export class VaDto implements VA {
  @IsString()
  @IsOptional()
  lastname?: string;

  @IsString()
  @IsOptional()
  firstname?: string;

  @IsString()
  @IsOptional()
  @Matches(VA_FORMAT)
  vaNumber?: string;
}
