import { ApiProperty } from "@nestjs/swagger";
import { VaDtoResponse as VA, VAStatus } from "@overrun/types";

export class VaDtoResponse implements VA {
  @ApiProperty({ enum: VAStatus })
  status: VAStatus;
  @ApiProperty()
  vaMail?: string;
}
