import {
  ConflictException,
  ForbiddenException,
  HttpException,
  Injectable,
  NotFoundException,
} from "@nestjs/common";
import { PrismaService } from "../prisma.service";
import { VaDto } from "./dto/va.request.dto";
import { InscriptionStatus, VAStatus } from "@overrun/types";
import { SEC_TO_MS } from "@overrun/constant";
import { CommonService } from "src/common/common.service";
import { VaDtoResponse } from "./dto/va.response.dto";

const VA_URL = `${process.env.EDB_SSO_ENDPOINT}/auth/realms/${process.env.EDB_REALM}/protocol/openid-connect/token`;

@Injectable()
export class VaService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly commonService: CommonService,
  ) {}

  private static ACCEPTED_MEMBERSHIPS: string[] = ["VAvantages 2024-25"];

  private accessToken: string;
  private tokenExpiration: Date;

  async registerVa(dto: VaDto, save: boolean): Promise<VaDtoResponse> {
    //to do get connected user
    const connectedUserId = 1;

    const inscriptionId = await this.checkInscription(connectedUserId);

    const response = await this.checkVa(dto);

    if (response.status !== VAStatus.VALIDATED) {
      return response;
    }

    await this.checkUnique(response.vaMail, dto.vaNumber);

    if (save) {
      this.prisma.vA.create({
        data: {
          va: dto.vaNumber,
          inscriptionId: inscriptionId,
          validated: true,
          vaEmail: response.vaMail,
        },
      });
    }

    return response;
  }

  async confirmVa(email: string): Promise<VaDtoResponse> {
    //to do get connected user
    const connectedUserId = 1;
    const currentInscriptionId = await this.checkInscription(connectedUserId);

    await this.checkUnique(email, null);

    await this.prisma.vA.create({
      data: {
        va: "",
        inscriptionId: currentInscriptionId,
        validated: true,
        vaEmail: email,
      },
    });

    return {
      status: VAStatus.VALIDATED,
      vaMail: email,
    };
  }

  private async checkInscription(userId: number) {
    const currentInscriptionId =
      await this.commonService.getActiveInscription(userId);

    if (!currentInscriptionId) {
      throw new NotFoundException(
        `Pas d'inscription en cours pour l'athlète #${userId}`,
      );
    }

    const currentInscription = await this.prisma.inscription.findUnique({
      where: {
        id: currentInscriptionId,
      },
      select: {
        id: true,
        va: true,
        status: true,
      },
    });

    if (currentInscription.status === InscriptionStatus.CANCEL_ASKED) {
      throw new ForbiddenException(`Inscription en cours d'annulation`);
    }

    if (currentInscription.va !== null) {
      throw new ConflictException("Inscription déjà associée à une VA");
    }
    return currentInscriptionId;
  }

  private async checkUnique(vaMail: string, vaNumber: string) {
    const OR = [];
    if (vaNumber) {
      OR.push({ va: vaNumber });
    }
    if (vaMail) {
      OR.push({ vaEmail: vaMail });
    }
    const alreadyUsed = await this.prisma.vA.findMany({
      where: {
        OR,
        validated: true,
        inscription: {
          status: { not: InscriptionStatus.CANCELLED },
        },
      },
    });

    if (alreadyUsed.length > 0) {
      throw new ConflictException("Le code VA a déjà été utilisé");
    }

    return;
  }

  private async checkVa({
    firstname,
    lastname,
    vaNumber,
  }: VaDto): Promise<VaDtoResponse> {
    if (!this.accessToken || new Date() > this.tokenExpiration) {
      await this.getToken();
    }

    const params = new URLSearchParams([["slugify", "true"]]);

    if (vaNumber) {
      params.append("card_number", vaNumber);
    } else {
      params.append("first_name", firstname);
      params.append("last_name", lastname);
    }

    const response = await fetch(
      `${process.env.EDB_VA_ENDPOINT}/va_check?${params.toString()}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${this.accessToken}`,
        },
      },
    );

    if (!response.ok) {
      if (response.status >= 500) {
        throw new HttpException("L'API du BDE a renvoyé une erreur", 424);
      }

      if (response.status >= 400) {
        return { status: VAStatus.NOT_MEMBER };
      }
    }

    const json = await response.json();

    if (json.members.length === 0) {
      return { status: VAStatus.NOT_MEMBER };
    }

    if (json.members.length > 1) {
      return { status: VAStatus.NOT_FOUND };
    }

    const memberships = json.members[0].memberships?.map(({ name }) => name);

    if (
      VaService.ACCEPTED_MEMBERSHIPS.every((m) => !memberships?.includes(m))
    ) {
      return { status: VAStatus.INVALID };
    }

    return {
      status: VAStatus.VALIDATED,
      vaMail: json.members[0].email,
    };
  }

  private async getToken(): Promise<void> {
    const response = await fetch(VA_URL, {
      method: "POST",
      body: new URLSearchParams({
        grant_type: "client_credentials",
        client_id: process.env.EDB_CLIENT_ID,
        client_secret: process.env.EDB_CLIENT_SECRET,
      }),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    });

    const json = await response.json();
    this.accessToken = json.access_token;
    this.tokenExpiration = new Date(
      new Date().getTime() + json.expires_in * SEC_TO_MS,
    );
  }
}
