import { Module } from "@nestjs/common";
import { VaController } from "./va.controller";
import { VaService } from "./va.service";
import { CommonModule } from "src/common/common.module";

@Module({
  imports: [CommonModule],
  controllers: [VaController],
  providers: [VaService],
})
export class VaModule {}
