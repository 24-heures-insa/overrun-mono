import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from "@nestjs/swagger";
import { Body, Controller, Get, Post, Query, UseGuards } from "@nestjs/common";
import { VaService } from "./va.service";
import { VaDto } from "./dto/va.request.dto";
import { oidcRoles } from "@overrun/types";
import { RolesGuard, ZitadelAuthGuard } from "src/authentication/guards";
import { Roles } from "src/authentication/decorators";
import { VaDtoResponse } from "./dto/va.response.dto";

@UseGuards(ZitadelAuthGuard, RolesGuard)
@ApiBearerAuth()
@ApiTags("va")
@Controller("va")
@ApiBadRequestResponse({
  description: "Bad Request va",
})
export class VaController {
  constructor(private readonly vaService: VaService) {}

  @Get()
  @Roles(oidcRoles.ADMIN, oidcRoles.ATHLETE)
  @ApiResponse({
    status: 200,
    description: "Find VA from first and last names",
    type: VaDtoResponse,
  })
  @ApiQuery({
    name: "firstname",
    type: String,
    required: true,
  })
  @ApiQuery({
    name: "lastname",
    type: String,
    required: true,
  })
  findVa(
    @Query("firstname") firstname,
    @Query("lastname") lastname,
  ): Promise<VaDtoResponse> {
    return this.vaService.registerVa({ firstname, lastname }, false);
  }

  @Post("confirm")
  @Roles(oidcRoles.ADMIN, oidcRoles.ATHLETE)
  @ApiBody({
    type: String,
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: "Link VA to inscription with a given mail",
    type: VaDtoResponse,
  })
  confirm(@Body("email") email: string) {
    return this.vaService.confirmVa(email);
  }

  @Post("register")
  @Roles(oidcRoles.ADMIN, oidcRoles.ATHLETE)
  @ApiBody({
    type: VaDto,
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: "Find va from Card number and saves it",
    type: VaDtoResponse,
  })
  registerVa(@Body() data: VaDto): Promise<VaDtoResponse> {
    return this.vaService.registerVa(data, true);
  }
}
