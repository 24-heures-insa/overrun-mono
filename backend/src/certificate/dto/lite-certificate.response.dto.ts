export class CertificateLiteResponseDto {
  id: number;
  filename: string;
  status: CertificateStatusDto;
}

export const CertificateStatus: {
  VALIDATED: "VALIDATED";
  PENDING: "PENDING";
  REFUSED: "REFUSED";
} = {
  VALIDATED: "VALIDATED",
  PENDING: "PENDING",
  REFUSED: "REFUSED",
};

export type CertificateStatusDto =
  (typeof CertificateStatus)[keyof typeof CertificateStatus];
