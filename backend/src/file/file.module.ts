import { Module } from "@nestjs/common";
import { FileController } from "./file.controller";
import { FileService } from "./file.service";
import { UserModule } from "src/user/user.module";
import { CommonModule } from "src/common/common.module";

@Module({
  imports: [UserModule, CommonModule],
  controllers: [FileController],
  providers: [FileService],
  exports: [FileService],
})
export class FileModule {}
