import {
  ForbiddenException,
  Injectable,
  NotFoundException,
  StreamableFile,
} from "@nestjs/common";
import { PrismaService } from "src/prisma.service";
import { CertificateStatus, InscriptionStatus } from "@overrun/types";
import { join } from "path";
import { createReadStream, existsSync, unlinkSync } from "fs";
import { UserService } from "src/user/user.service";
import { MyInfoResponseDto } from "src/user/dto/my-info.response.dto";
import {
  FILE_EXTENSIONS,
  getFileExtensionFromFilename,
} from "@overrun/constant";
import { CommonService } from "src/common/common.service";

@Injectable()
export class FileService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly userService: UserService,
    private readonly commonService: CommonService,
  ) {}

  private static allExtensions: string = Object.values(FILE_EXTENSIONS)
    .flatMap(({ extension }) => extension)
    .join("|");
  static allFileRegex = new RegExp(".(" + FileService.allExtensions + ")$");

  private static imgExtensions: string = Object.values(FILE_EXTENSIONS)
    .filter(({ isImg }) => isImg)
    .flatMap(({ extension }) => extension)
    .join("|");
  static imgFileRegex = new RegExp(".(" + FileService.imgExtensions + ")$");

  static PATH_PUBLIC = "/public/";
  static PATH_RICH_EDITOR = FileService.PATH_PUBLIC + "richEditor/";

  async uploadCertificate(filename: string): Promise<MyInfoResponseDto> {
    //TODO
    const userId = 1;

    const inscriptionId = await this.commonService.getActiveInscription(userId);
    if (!inscriptionId) {
      throw new NotFoundException(
        `Pas d'inscription en cours pour l'athlète #${userId}`,
      );
    }

    const { certificate, status } = await this.prisma.inscription.findUnique({
      where: {
        id: inscriptionId,
      },
      select: {
        certificate: true,
        status: true,
      },
    });

    if (status === InscriptionStatus.CANCEL_ASKED) {
      throw new ForbiddenException(`Inscription en cours d'annulation`);
    }

    if (certificate) {
      this.deleteFile(certificate.filename);
    }

    await this.prisma.certificate.upsert({
      where: {
        inscriptionId,
      },
      update: {
        filename,
        uploadedAt: new Date(),
        statusUpdatedAt: new Date(),
        status: CertificateStatus.PENDING,
      },
      create: {
        filename,
        status: CertificateStatus.PENDING,
        inscriptionId,
      },
    });

    return this.userService.getMyInfo();
  }

  async getCertificateByName(filename: string) {
    return this.streamFile(filename);
  }

  async getCertificateFromUserId(userId: number) {
    const [{ id: currentInscriptionId }] =
      await this.prisma.inscription.findMany({
        where: {
          status: {
            not: InscriptionStatus.CANCELLED,
          },
          athleteId: userId,
        },
        select: {
          id: true,
        },
      });

    const { filename } = await this.prisma.certificate.findUnique({
      where: {
        inscriptionId: currentInscriptionId,
      },
    });

    return filename;
  }

  async streamCertificateByUserId(userId: number): Promise<StreamableFile> {
    const filename = await this.getCertificateFromUserId(userId);
    if (!filename) {
      throw new NotFoundException("Certificate not found");
    }
    return this.streamFile(filename);
  }

  async streamCertificate(certificateId: number): Promise<StreamableFile> {
    const { filename } = await this.prisma.certificate.findUnique({
      where: {
        id: certificateId,
      },
    });
    if (!filename) {
      throw new NotFoundException("Certificate not found");
    }
    return this.streamFile(filename);
  }

  streamFile(
    filename: string,
    dest: string = FileService.PATH_PUBLIC,
  ): StreamableFile {
    const filePath = join(process.cwd(), dest, filename);
    // nosemgrep
    if (!existsSync(filePath)) {
      throw new NotFoundException("File not found");
    }
    const { typeMime } = getFileExtensionFromFilename(filename);
    // nosemgrep
    const file = createReadStream(filePath);
    return new StreamableFile(file, { type: typeMime });
  }

  deleteFile(filename: string, dest: string = FileService.PATH_PUBLIC) {
    const filePath = join(process.cwd(), dest, filename);
    // nosemgrep
    if (!existsSync(filePath)) {
      return;
    }
    unlinkSync(filePath);
  }
}
