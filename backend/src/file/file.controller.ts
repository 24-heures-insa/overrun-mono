import {
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Query,
  StreamableFile,
  UploadedFile,
  UseInterceptors,
  UnsupportedMediaTypeException,
  ParseFilePipe,
  FileTypeValidator,
  HttpStatus,
  UseGuards,
} from "@nestjs/common";
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiResponse,
  ApiTags,
} from "@nestjs/swagger";
import { diskStorage } from "multer";
import { FileInterceptor } from "@nestjs/platform-express/multer";
import { join } from "path";
import { randomUUID } from "crypto";
import "multer";
import { FileUploadDto } from "./dto/file-upload.request.dto";
import { FileService } from "./file.service";
import {
  Filetype,
  FiletypeType,
  MyInfoResponseDto,
} from "src/user/dto/my-info.response.dto";
import { RolesGuard, ZitadelAuthGuard } from "src/authentication/guards";
import { Public, Roles } from "src/authentication/decorators";
import { oidcRoles } from "@overrun/types";

@UseGuards(ZitadelAuthGuard, RolesGuard)
@ApiBearerAuth()
@ApiTags("files")
@Controller("files")
@ApiBadRequestResponse({
  description: "Bad Request files",
})
export class FileController {
  constructor(private readonly fileService: FileService) {}

  @Post("certificate")
  @Roles(oidcRoles.ADMIN, oidcRoles.ATHLETE)
  @UseInterceptors(
    FileInterceptor("file", {
      storage: diskStorage({
        destination: join(process.cwd(), FileService.PATH_PUBLIC),
        filename: (_, file, callback) => {
          const uuid = randomUUID();
          const extension = file.originalname.split(".").at(-1) ?? "pdf";
          callback(null, `${uuid}.${extension}`);
        },
      }),
      fileFilter: (_, file, cb) => {
        const isValid = FileService.allFileRegex.test(file.originalname);
        if (isValid) {
          cb(null, true);
        } else {
          cb(new UnsupportedMediaTypeException(), false);
        }
      },
    }),
  )
  @ApiConsumes("multipart/form-data")
  @ApiResponse({
    status: 201,
    description: "Upload certificate",
  })
  @ApiBody({
    description: "Certificate file",
    type: FileUploadDto,
  })
  uploadCertificate(
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new FileTypeValidator({ fileType: FileService.allFileRegex }),
        ],
        errorHttpStatusCode: HttpStatus.UNSUPPORTED_MEDIA_TYPE,
      }),
    )
    file: Express.Multer.File,
  ): Promise<MyInfoResponseDto> {
    return this.fileService.uploadCertificate(file.filename);
  }

  @Post("richEditor")
  @Roles(oidcRoles.ADMIN)
  @UseInterceptors(
    FileInterceptor("image", {
      storage: diskStorage({
        destination: join(process.cwd(), FileService.PATH_RICH_EDITOR),
        filename: (_, file, callback) => {
          const uuid = randomUUID();
          const extension = file.originalname.split(".").at(-1) ?? "png";
          callback(null, `${uuid}.${extension}`);
        },
      }),
      fileFilter: (_, file, cb) => {
        const isValid = FileService.imgFileRegex.test(file.originalname);
        if (isValid) {
          cb(null, true);
        } else {
          cb(new UnsupportedMediaTypeException(), false);
        }
      },
    }),
  )
  @ApiConsumes("multipart/form-data")
  @ApiResponse({
    status: 201,
    description: "Upload image",
  })
  @ApiBody({
    description: "Images for RichEditor",
    type: FileUploadDto,
  })
  uploaderRichEditorImages(
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new FileTypeValidator({ fileType: FileService.imgFileRegex }),
        ],
      }),
    )
    file: Express.Multer.File,
  ): { url: string } {
    return { url: file.filename };
  }

  @Get("certificate")
  @Roles(oidcRoles.ADMIN, oidcRoles.ATHLETE)
  @ApiResponse({
    status: 200,
    description: "Get connnected user certificate",
  })
  getCertificateFile(): Promise<StreamableFile> {
    return this.fileService.streamCertificateByUserId(1);
  }

  @Public()
  @Get("richEditor/:name")
  @ApiResponse({
    status: 200,
    description: "Fetch an image from its server name",
  })
  getImg(@Param("name") name: string): StreamableFile {
    return this.fileService.streamFile(name, FileService.PATH_RICH_EDITOR);
  }

  @Get(":fileId")
  @Roles(oidcRoles.ATHLETE)
  @ApiResponse({
    status: 200,
    description: "Fetch a file from id and type",
  })
  getFile(
    @Param("fileId", ParseIntPipe) fileId: number,
    @Query("type") filetype: FiletypeType,
  ): Promise<StreamableFile> {
    switch (filetype) {
      case Filetype.CERTIFICATE:
        return this.fileService.streamCertificate(fileId);
      default:
        console.log("WIP");
        break;
    }
  }
}
