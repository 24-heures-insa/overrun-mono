import { Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma.service";
import { CompleteDisciplineResponseDto } from "./dto/complete-discipline.response.dto";
import { CreateDisciplineRequestDto } from "./dto/create-discipline.request.dto";
import { UpdateDisciplineRequestDto } from "./dto/update-discipline.request.dto";

const selection = {
  id: true,
  name: true,
  description: true,
  raceDuration: {
    select: {
      id: true,
      duration: true,
      race: {
        select: {
          id: true,
          name: true,
        },
      },
    },
  },
};

@Injectable()
export class DisciplineService {
  constructor(private readonly prisma: PrismaService) {}

  async getById(id: number): Promise<CompleteDisciplineResponseDto> {
    return await this.prisma.discipline.findUnique({
      where: { id },
      select: selection,
    });
  }

  async getByEditionId(
    editionId: number,
  ): Promise<CompleteDisciplineResponseDto[]> {
    return await this.prisma.discipline.findMany({
      where: {
        editionId: editionId,
      },
      select: selection,
      orderBy: {
        id: "asc",
      },
    });
  }

  async getAll(): Promise<CompleteDisciplineResponseDto[]> {
    return await this.prisma.discipline.findMany({
      orderBy: {
        id: "asc",
      },
      select: selection,
    });
  }

  async create(
    disciplineCreation: CreateDisciplineRequestDto,
  ): Promise<CompleteDisciplineResponseDto> {
    return await this.prisma.discipline.create({
      data: disciplineCreation,
      select: selection,
    });
  }

  async update(
    id: number,
    d: UpdateDisciplineRequestDto,
  ): Promise<CompleteDisciplineResponseDto> {
    const discipline = await this.prisma.discipline.findUnique({
      where: { id },
    });
    const data = {
      name: d.name || discipline.name,
      description: d.description || discipline.description,
    };
    return await this.prisma.discipline.update({
      where: { id },
      select: selection,
      data: data,
    });
  }

  async deleteById(id: number) {
    await this.prisma.discipline.delete({
      where: { id },
    });
  }

  async deleteByIdList(ids: number[]) {
    await this.prisma.discipline.deleteMany({
      where: {
        id: {
          in: ids,
        },
      },
    });
  }
}
