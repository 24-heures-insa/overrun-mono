import { IsInt, IsNotEmpty, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";
import { CreateDisciplineRequest } from "@overrun/types";

export class CreateDisciplineRequestDto implements CreateDisciplineRequest {
  @ApiProperty({
    required: true,
    description: "The name of the discipline",
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    required: true,
    description: "The description of the discipline",
    type: String,
  })
  @IsNotEmpty()
  @IsString()
  description: string;

  @ApiProperty({
    required: true,
    description: "The edition id of the discipline",
    type: Number,
  })
  @IsNotEmpty()
  @IsInt()
  editionId: number;
}
