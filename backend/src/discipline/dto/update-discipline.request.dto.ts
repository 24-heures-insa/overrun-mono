import { IsOptional, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";
import { UpdateDisciplineRequest } from "@overrun/types";

export class UpdateDisciplineRequestDto implements UpdateDisciplineRequest {
  @ApiProperty({
    required: false,
    description: "The name of the discipline",
    type: String,
  })
  @IsOptional()
  @IsString()
  name!: string;

  @ApiProperty({
    required: false,
    description: "The description of the discipline",
    type: String,
  })
  @IsOptional()
  @IsString()
  description!: string;
}
