import { CompleteDisciplineResponse, DisciplineDuration } from "@overrun/types";
import { IsArray, IsInt, IsNotEmpty, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class CompleteDisciplineResponseDto
  implements CompleteDisciplineResponse
{
  @ApiProperty({
    required: true,
    description: "The id of the discipline",
    type: Number,
  })
  @IsInt()
  @IsNotEmpty()
  id: number;

  @ApiProperty({
    required: true,
    description: "The name of the discipline",
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    required: true,
    description: "The description of the discipline",
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  description: string;

  @ApiProperty({
    required: true,
    description: "The races where the discipline occurs",
    type: Object,
    isArray: true,
  })
  @IsNotEmpty()
  @IsArray()
  raceDuration: DisciplineDuration[];
}
