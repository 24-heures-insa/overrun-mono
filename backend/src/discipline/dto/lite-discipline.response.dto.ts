import { ApiProperty } from "@nestjs/swagger";
import { LiteDisciplineResponse } from "@overrun/types";
import { IsInt, IsNotEmpty, IsString } from "class-validator";

export class LiteDisciplineResponseDto implements LiteDisciplineResponse {
  @ApiProperty({
    required: true,
    description: "The id of the discipline",
    type: Number,
  })
  @IsInt()
  @IsNotEmpty()
  id: number;

  @ApiProperty({
    required: true,
    description: "The name of the discipline",
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    required: true,
    description: "The description of the discipline",
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  description: string;
}
