import { ApiProperty } from "@nestjs/swagger";
import { IsInt, IsNotEmpty, IsString } from "class-validator";

export class DisciplineDurationDto {
  @ApiProperty({
    required: true,
    description: "The id of the discipline",
    type: Number,
  })
  @IsInt()
  @IsNotEmpty()
  id: number;

  @ApiProperty({
    required: true,
    description: "The name of the discipline",
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    required: true,
    description: "The duration of the discipline for a given race",
    type: Number,
  })
  @IsInt()
  @IsNotEmpty()
  duration: number;
}
