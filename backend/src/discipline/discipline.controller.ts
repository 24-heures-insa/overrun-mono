import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from "@nestjs/swagger";
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  UseGuards,
} from "@nestjs/common";
import { DisciplineService } from "./discipline.service";
import { CompleteDisciplineResponseDto } from "./dto/complete-discipline.response.dto";
import { CreateDisciplineRequestDto } from "./dto/create-discipline.request.dto";
import { UpdateDisciplineRequestDto } from "./dto/update-discipline.request.dto";
import { RolesGuard, ZitadelAuthGuard } from "src/authentication/guards";
import { Roles } from "src/authentication/decorators";
import { oidcRoles } from "@overrun/types";

@UseGuards(ZitadelAuthGuard, RolesGuard)
@ApiBearerAuth()
@ApiTags("disciplines")
@Controller("disciplines")
@ApiBadRequestResponse({
  description: "Bad Request",
})
export class DisciplineController {
  constructor(private readonly disciplineService: DisciplineService) {}

  @Get(":id")
  @Roles(oidcRoles.ADMIN, oidcRoles.ATHLETE)
  @ApiResponse({
    status: 200,
    description: "Get discipline from id",
    type: CompleteDisciplineResponseDto,
  })
  getDiscipline(
    @Param("id", ParseIntPipe) id: number,
  ): Promise<CompleteDisciplineResponseDto> {
    return this.disciplineService.getById(id);
  }

  @Get()
  @Roles(oidcRoles.ADMIN, oidcRoles.ATHLETE)
  @ApiQuery({
    name: "editionId",
    type: Number,
    description:
      "Optional. If specified, only the edition disciplines are returned.",
    required: false,
  })
  @ApiResponse({
    status: 200,
    description:
      "Get all disciplines from a given edition. If null, get all disciplines",
    type: CompleteDisciplineResponseDto,
  })
  getDisciplines(
    @Query("editionId") editionId?: string,
  ): Promise<CompleteDisciplineResponseDto[]> {
    return editionId
      ? this.disciplineService.getByEditionId(parseInt(editionId))
      : this.disciplineService.getAll();
  }

  @Post()
  @Roles(oidcRoles.ADMIN)
  @ApiResponse({
    status: 201,
    description: "Creates a new discipline",
    type: CompleteDisciplineResponseDto,
  })
  @ApiBody({
    type: CreateDisciplineRequestDto,
    description: "Discipline to be created",
    required: true,
  })
  createDiscipline(
    @Body() discipline: CreateDisciplineRequestDto,
  ): Promise<CompleteDisciplineResponseDto> {
    return this.disciplineService.create(discipline);
  }

  @Patch(":id")
  @Roles(oidcRoles.ADMIN)
  @ApiResponse({
    status: 200,
    description: "Update an existing discipline",
    type: CompleteDisciplineResponseDto,
  })
  @ApiBody({
    type: UpdateDisciplineRequestDto,
    description: "Fields that can be updated: name, description.",
    required: true,
  })
  updateDiscipline(
    @Param("id", ParseIntPipe) id: number,
    @Body() discipline: UpdateDisciplineRequestDto,
  ): Promise<CompleteDisciplineResponseDto> {
    return this.disciplineService.update(id, discipline);
  }

  @Delete(":id")
  @Roles(oidcRoles.ADMIN)
  @ApiResponse({
    status: 200,
    description: "Deletes discipline with the given id",
  })
  deleteDiscipline(@Param("id", ParseIntPipe) id: number): Promise<void> {
    return this.disciplineService.deleteById(id);
  }

  @Delete()
  @Roles(oidcRoles.ADMIN)
  @ApiResponse({
    status: 200,
    description: "Deletes all disciplines within the given id list",
  })
  @ApiQuery({
    name: "toDelete",
    description: "List of ids of disciplines to be deleted",
    required: true,
    type: [Number],
  })
  deleteMultipleDisciplines(
    @Query("toDelete", new ParseArrayPipe({ items: Number }))
    toDelete: number[],
  ): Promise<void> {
    return this.disciplineService.deleteByIdList(toDelete);
  }
}
