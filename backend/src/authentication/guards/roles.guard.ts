import {
  CanActivate,
  ExecutionContext,
  Injectable,
  Logger,
} from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { ROLES_METADATA } from "../decorators/roles.decorator";
import { oidcRolesClaims } from "@overrun/types";
import { IS_PUBLIC_KEY } from "../decorators";

@Injectable()
export class RolesGuard implements CanActivate {
  private readonly logger: Logger = new Logger(RolesGuard.name);

  constructor(private reflector: Reflector) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);

    if (isPublic) {
      return true;
    }

    const roles = this.reflector.getAllAndOverride<string[]>(ROLES_METADATA, [
      context.getHandler(),
      context.getClass(),
    ]);

    if (!roles) {
      return false;
    }

    const { user } = context.switchToHttp().getRequest();
    if (!user) {
      return false;
    }
    // eslint-disable-next-line security/detect-object-injection
    const rolesObj = user[oidcRolesClaims];

    if (!rolesObj) {
      this.logger.debug("User does not contains any role");
      return false;
    }

    const userRoles = Object.keys(rolesObj);
    return roles.some((x) => userRoles.includes(x));
  }
}
