import { SetMetadata } from "@nestjs/common";
import { oidcRoles } from "@overrun/types";

export const ROLES_METADATA = "zest_roles";

export const Roles = (...args: oidcRoles[]) =>
  SetMetadata(ROLES_METADATA, args);
