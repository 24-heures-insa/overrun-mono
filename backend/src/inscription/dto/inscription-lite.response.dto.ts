import { CertificateLiteResponseDto } from "src/certificate/dto/lite-certificate.response.dto";
import { PaymentLiteResponseDto } from "src/payment/dto/payment-lite.response.dto";
import { RaceLiteResponseDto } from "src/race/dto/race-lite.response.dto";
import { TeammateResponseDto } from "src/team/dto/teammate.response.dto";

export class InscriptionLiteResponseDto {
  id: number;
  status: InscriptionStatusDto;
  racingbib: number;
  teamMembers?: TeammateResponseDto[];
  teamName?: string;
  isAdmin?: boolean;
  race: RaceLiteResponseDto;
  va: boolean;
  certificate: CertificateLiteResponseDto;
  payment: PaymentLiteResponseDto;
}

export const InscriptionStatus: {
  PENDING: "PENDING";
  VALIDATED: "VALIDATED";
  CANCELLED: "CANCELLED";
  CANCEL_ASKED: "CANCEL_ASKED";
} = {
  PENDING: "PENDING",
  VALIDATED: "VALIDATED",
  CANCELLED: "CANCELLED",
  CANCEL_ASKED: "CANCEL_ASKED",
};
export type InscriptionStatusDto =
  (typeof InscriptionStatus)[keyof typeof InscriptionStatus];
