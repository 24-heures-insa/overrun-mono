import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsBoolean, IsNumber, IsOptional, IsString } from "class-validator";

class JoinTeamDto {
  @ApiProperty({
    type: Number,
    required: true,
  })
  @IsNumber()
  teamId: number;

  @ApiProperty({
    type: String,
    required: true,
  })
  @IsString()
  teamToken: string;

  @ApiPropertyOptional({
    type: Boolean,
  })
  @IsBoolean()
  isAdmin: boolean;
}

export class InscriptionCreationDto {
  @ApiProperty({
    required: true,
    type: Number,
  })
  @IsNumber()
  raceId: number;

  @ApiPropertyOptional({
    type: JoinTeamDto,
  })
  @IsOptional()
  team?: JoinTeamDto;
}
