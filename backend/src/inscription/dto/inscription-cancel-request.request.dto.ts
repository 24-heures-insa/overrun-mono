import { IsOptional, IsString } from "class-validator";

export class InscriptionCancelRequestDto {
  @IsString()
  @IsOptional()
  comment: string;
}
