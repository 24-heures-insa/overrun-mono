import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma.service";
import { InscriptionCreationDto } from "./dto/inscription-creation.request.dto";
import { MyInfoResponseDto } from "src/user/dto/my-info.response.dto";
import { InscriptionStatus } from "@overrun/types";
import { UserService } from "src/user/user.service";
import { timingSafeEqual } from "crypto";
import { PaymentStatus } from "@overrun/types";
import { InscriptionCancelRequestDto } from "./dto/inscription-cancel-request.request.dto";
import { CommonService } from "src/common/common.service";

@Injectable()
export class InscriptionService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly userService: UserService,
    private readonly commonService: CommonService,
  ) {}

  async register(dto: InscriptionCreationDto): Promise<MyInfoResponseDto> {
    //to do à enlever avec zitadel
    const userId = 1;
    //vérification du token
    if (dto.team && !dto.team.isAdmin) {
      const { password: token } = await this.prisma.team.findUnique({
        where: {
          id: dto.team.teamId,
        },
      });
      if (!this.checkToken(dto.team.teamToken, token)) {
        console.log("mauvais mot de passe");
        return null;
      }
    }

    const inscription = await this.prisma.inscription.create({
      data: {
        athleteId: userId,
        raceId: dto.raceId,
        status: InscriptionStatus.PENDING,
        teamId: dto.team?.teamId ?? null,
        teamAdminId: dto.team?.isAdmin ? (dto.team?.teamId ?? null) : null,
      },
    });

    //todo
    await this.prisma.payment.create({
      data: {
        inscriptionId: inscription.id,
        raceAmount: 0,
        totalAmount: 0,
        status: PaymentStatus.NOT_STARTED,
      },
    });

    return this.userService.getMyInfo();
  }

  async saveCancelRequest({ comment }: InscriptionCancelRequestDto) {
    //todo récupération de l'utilisateur connecté
    const userId = 1;

    //récupération de l'inscription en cours
    const inscriptionId = await this.commonService.getActiveInscription(userId);
    if (!inscriptionId) {
      throw new HttpException(
        "pas d'inscription en cours",
        HttpStatus.BAD_REQUEST,
      );
    }

    const motif =
      "Demande d'annulation." + (comment ? " Motif : " + comment : "");
    const dataComment = {
      comment: motif,
      authorId: userId,
      inscriptionId,
    };

    await this.prisma.$transaction([
      this.prisma.comment.create({ data: dataComment }),
      this.prisma.inscription.update({
        data: {
          status: InscriptionStatus.CANCEL_ASKED,
        },
        where: {
          id: inscriptionId,
        },
      }),
      this.prisma.payment.updateMany({
        where: {
          inscriptionId,
          status: PaymentStatus.VALIDATED,
        },
        data: {
          status: PaymentStatus.REFUNDING,
        },
      }),
    ]);
    return this.userService.getMyInfo();
  }

  private checkToken(input: string, token: string): boolean {
    return timingSafeEqual(
      Buffer.from(input, "utf-8"),
      Buffer.from(token, "utf-8"),
    );
  }
}
