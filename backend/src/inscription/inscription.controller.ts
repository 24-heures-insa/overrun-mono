import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiResponse,
  ApiTags,
} from "@nestjs/swagger";
import { Body, Controller, Post, UseGuards } from "@nestjs/common";
import { InscriptionService } from "./inscription.service";
import { InscriptionCreationDto } from "./dto/inscription-creation.request.dto";
import { MyInfoResponseDto } from "src/user/dto/my-info.response.dto";
import { InscriptionCancelRequestDto } from "./dto/inscription-cancel-request.request.dto";
import { Public, Roles } from "src/authentication/decorators";
import { RolesGuard, ZitadelAuthGuard } from "src/authentication/guards";
import { oidcRoles } from "@overrun/types";

@ApiTags("inscriptions")
@Controller("inscriptions")
@ApiBadRequestResponse({
  description: "Bad Request inscriptions",
})
export class InscriptionController {
  constructor(private readonly inscriptionService: InscriptionService) {}

  @Post("register")
  @Public()
  @ApiResponse({
    status: 200,
    description: "Create an inscription between a user and a race",
    type: MyInfoResponseDto,
  })
  @ApiBody({
    required: true,
    type: InscriptionCreationDto,
  })
  register(@Body() data: InscriptionCreationDto): Promise<MyInfoResponseDto> {
    return this.inscriptionService.register(data);
  }

  @Post("cancelRequest")
  @UseGuards(ZitadelAuthGuard, RolesGuard)
  @Roles(oidcRoles.ATHLETE)
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: "User request for cancelling their inscription",
    type: MyInfoResponseDto,
  })
  @ApiBody({
    required: true,
    type: InscriptionCancelRequestDto,
  })
  cancelRequest(@Body() data: InscriptionCancelRequestDto) {
    return this.inscriptionService.saveCancelRequest(data);
  }
}
