import { Module } from "@nestjs/common";
import { InscriptionController } from "./inscription.controller";
import { InscriptionService } from "./inscription.service";
import { UserModule } from "src/user/user.module";
import { CommonModule } from "src/common/common.module";

@Module({
  imports: [UserModule, CommonModule],
  controllers: [InscriptionController],
  providers: [InscriptionService],
})
export class InscriptionModule {}
