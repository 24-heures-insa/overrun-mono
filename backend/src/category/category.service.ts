import { Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma.service";
import { CompleteCategoryResponseDto } from "./dto/complete-category.response.dto";
import { CreateCategoryRequestDto } from "./dto/create-category.request.dto";
import { UpdateCategoryRequestDto } from "./dto/update-category.request.dto";

const selection = {
  id: true,
  name: true,
  description: true,
  maxTeamMembers: true,
  minTeamMembers: true,
  races: {
    select: {
      id: true,
      name: true,
    },
  },
  editionId: true,
};

@Injectable()
export class CategoryService {
  constructor(private readonly prisma: PrismaService) {}

  async getById(id: number): Promise<CompleteCategoryResponseDto> {
    return await this.prisma.category.findUnique({
      where: { id },
      select: selection,
    });
  }

  async getByEditionId(
    editionId: number,
  ): Promise<CompleteCategoryResponseDto[]> {
    return await this.prisma.category.findMany({
      where: {
        editionId: editionId,
      },
      select: selection,
      orderBy: {
        id: "asc",
      },
    });
  }

  async getAll(): Promise<CompleteCategoryResponseDto[]> {
    return await this.prisma.category.findMany({
      orderBy: {
        id: "asc",
      },
      select: selection,
    });
  }

  async create(
    c: CreateCategoryRequestDto,
  ): Promise<CompleteCategoryResponseDto> {
    return await this.prisma.category.create({
      data: {
        name: c.name,
        description: c.description,
        maxTeamMembers: c.maxTeamMembers,
        minTeamMembers: c.minTeamMembers,
        editionId: c.editionId,
      },
      select: selection,
    });
  }

  async update(
    id: number,
    c: UpdateCategoryRequestDto,
  ): Promise<CompleteCategoryResponseDto> {
    const category = await this.prisma.category.findUnique({
      where: { id },
    });
    const data = {
      name: c.name || category.name,
      description: c.description || category.description,
      minTeamMembers: c.minTeamMembers || category.minTeamMembers,
      maxTeamMembers: c.maxTeamMembers || category.maxTeamMembers,
    };
    return await this.prisma.category.update({
      where: { id },
      select: selection,
      data: data,
    });
  }

  async deleteById(id: number) {
    await this.prisma.category.delete({
      where: { id },
    });
  }

  async deleteByIdList(ids: number[]) {
    await this.prisma.category.deleteMany({
      where: {
        id: {
          in: ids,
        },
      },
    });
  }
}
