import { IsInt, IsNotEmpty, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";
import { LiteCategoryResponse } from "@overrun/types";

export class LiteCategoryResponseDto implements LiteCategoryResponse {
  @ApiProperty({
    required: true,
    description: "The id of the category",
    type: Number,
  })
  @IsInt()
  @IsNotEmpty()
  id: number;

  @ApiProperty({
    required: true,
    description: "The name of the category",
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    required: true,
    description: "The description of the category",
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  description: string;

  @ApiProperty({
    required: true,
    description: "Maximal number of athletes per team",
    type: Number,
  })
  @IsInt()
  @IsNotEmpty()
  maxTeamMembers: number;

  @ApiProperty({
    required: true,
    description: "Maximal number of athletes per team",
    type: Number,
  })
  @IsInt()
  @IsNotEmpty()
  minTeamMembers: number;
}
