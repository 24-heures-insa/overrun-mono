import { IsInt, IsNotEmpty, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";
import { CreateCategoryRequest } from "@overrun/types";

export class CreateCategoryRequestDto implements CreateCategoryRequest {
  @ApiProperty({
    required: true,
    description: "The name of the category",
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    required: true,
    description: "The description of the category",
    type: String,
  })
  @IsNotEmpty()
  @IsString()
  description: string;

  @ApiProperty({
    required: true,
    description: "The edition id of the category",
    type: Number,
  })
  @IsNotEmpty()
  @IsInt()
  editionId: number;

  @ApiProperty({
    required: true,
    description: "Maximal number of athlete per teams",
    type: Number,
  })
  @IsNotEmpty()
  @IsInt()
  maxTeamMembers: number;

  @ApiProperty({
    required: true,
    description: "Minimal number of athlete per teams",
    type: Number,
  })
  @IsNotEmpty()
  @IsInt()
  minTeamMembers: number;
}
