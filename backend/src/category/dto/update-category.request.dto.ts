import { IsInt, IsOptional, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";
import { UpdateDisciplineRequest } from "@overrun/types";

export class UpdateCategoryRequestDto implements UpdateDisciplineRequest {
  @ApiProperty({
    required: false,
    description: "The name of the category",
    type: String,
  })
  @IsString()
  @IsOptional()
  name?: string;

  @ApiProperty({
    required: false,
    description: "The description of the category",
    type: String,
  })
  @IsString()
  @IsOptional()
  description?: string;

  @ApiProperty({
    required: false,
    description: "Maximal number of athlete per teams",
    type: Number,
  })
  @IsOptional()
  @IsInt()
  maxTeamMembers?: number;

  @ApiProperty({
    required: false,
    description: "Minimal number of athlete per teams",
    type: Number,
  })
  @IsOptional()
  @IsInt()
  minTeamMembers?: number;
}
