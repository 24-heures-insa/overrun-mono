import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from "@nestjs/swagger";
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  UseGuards,
} from "@nestjs/common";
import { CategoryService } from "./category.service";
import { CompleteCategoryResponseDto } from "./dto/complete-category.response.dto";
import { CreateCategoryRequestDto } from "./dto/create-category.request.dto";
import { UpdateCategoryRequestDto } from "./dto/update-category.request.dto";
import { RolesGuard, ZitadelAuthGuard } from "src/authentication/guards";
import { Roles } from "src/authentication/decorators";
import { oidcRoles } from "@overrun/types";

@UseGuards(ZitadelAuthGuard, RolesGuard)
@ApiBearerAuth()
@ApiTags("categories")
@Controller("categories")
@ApiBadRequestResponse({
  description: "Bad Request",
})
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}

  @Get(":id")
  @Roles(oidcRoles.ADMIN, oidcRoles.ATHLETE)
  @ApiResponse({
    status: 200,
    description: "Get category from id",
    type: CompleteCategoryResponseDto,
  })
  getCategory(
    @Param("id", ParseIntPipe) id: number,
  ): Promise<CompleteCategoryResponseDto> {
    return this.categoryService.getById(id);
  }

  @Get()
  @Roles(oidcRoles.ADMIN, oidcRoles.ATHLETE)
  @ApiQuery({
    name: "editionId",
    type: Number,
    description:
      "Optional. If specified, only the edition categories are returned.",
    required: false,
  })
  @ApiResponse({
    status: 200,
    description:
      "Get all categories from a given edition. If null, get all categories",
    type: [CompleteCategoryResponseDto],
  })
  getCategories(
    @Query("editionId") editionId?: string,
  ): Promise<CompleteCategoryResponseDto[]> {
    return editionId
      ? this.categoryService.getByEditionId(parseInt(editionId))
      : this.categoryService.getAll();
  }

  @Post()
  @Roles(oidcRoles.ADMIN)
  @ApiResponse({
    status: 200,
    description: "Creates a new categorie",
    type: CompleteCategoryResponseDto,
  })
  @ApiBody({
    type: CreateCategoryRequestDto,
    description: "Category to be created",
    required: true,
  })
  createCategory(
    @Body() category: CreateCategoryRequestDto,
  ): Promise<CompleteCategoryResponseDto> {
    return this.categoryService.create(category);
  }

  @Patch(":id")
  @Roles(oidcRoles.ADMIN)
  @ApiResponse({
    status: 200,
    description: "Update an existing category",
    type: CompleteCategoryResponseDto,
  })
  @ApiBody({
    type: UpdateCategoryRequestDto,
    description:
      "Fields that can be updated: name, minTeamMembers, maxTeamMembers, description.",
    required: true,
  })
  updateCategory(
    @Param("id", ParseIntPipe) id: number,
    @Body() category: UpdateCategoryRequestDto,
  ): Promise<CompleteCategoryResponseDto> {
    return this.categoryService.update(id, category);
  }

  @Delete(":id")
  @Roles(oidcRoles.ADMIN)
  @ApiResponse({
    status: 200,
    description: "Deletes category with the given id",
  })
  deleteCategory(@Param("id", ParseIntPipe) id: number): Promise<void> {
    return this.categoryService.deleteById(id);
  }

  @Delete()
  @Roles(oidcRoles.ADMIN)
  @ApiResponse({
    status: 200,
    description: "Deletes all categories within the given id list",
  })
  @ApiQuery({
    name: "toDelete",
    description: "List of ids of categories to be deleted",
    required: true,
    type: [Number],
  })
  deleteMultipleCategories(
    @Query("toDelete", new ParseArrayPipe({ items: Number }))
    toDelete: number[],
  ): Promise<void> {
    return this.categoryService.deleteByIdList(toDelete);
  }
}
