import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiResponse,
  ApiTags,
} from "@nestjs/swagger";
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  UseGuards,
} from "@nestjs/common";
import { EditionService } from "./edition.service";
import { CompleteEditionResponseDto } from "./dto/complete-edition.response.dto";
import { CreateEditionRequestDto } from "./dto/create-edition.request.dto";
import { UpdateEditionRequestDto } from "./dto/update-edition.request.dto";
import { Public, Roles } from "src/authentication/decorators";
import { RolesGuard, ZitadelAuthGuard } from "src/authentication/guards";
import { oidcRoles } from "@overrun/types";

@ApiTags("editions")
@Controller("editions")
@ApiBadRequestResponse({
  description: "Bad Request",
})
export class EditionController {
  constructor(private readonly editionService: EditionService) {}

  @Get("active")
  @Public()
  @ApiResponse({
    status: 200,
    description: "Get active edition",
    type: CompleteEditionResponseDto,
  })
  getActiveEdition(): Promise<CompleteEditionResponseDto> {
    return this.editionService.getActiveEdition();
  }

  @Get(":id")
  @Public()
  @ApiResponse({
    status: 200,
    description: "Get edition from id",
    type: CompleteEditionResponseDto,
  })
  getEdition(
    @Param("id", ParseIntPipe) id: number,
  ): Promise<CompleteEditionResponseDto> {
    return this.editionService.getById(id);
  }

  @Get()
  @Public()
  @ApiResponse({
    status: 200,
    description: "Get all editions",
    type: [CompleteEditionResponseDto],
  })
  getEditions(): Promise<CompleteEditionResponseDto[]> {
    return this.editionService.getAll();
  }

  @Post()
  @UseGuards(ZitadelAuthGuard, RolesGuard)
  @Roles(oidcRoles.ADMIN)
  @ApiBearerAuth()
  @ApiResponse({
    status: 201,
    description: "Creates a new edition",
    type: CompleteEditionResponseDto,
  })
  @ApiBody({
    type: CreateEditionRequestDto,
    description: "Edition to be created",
    required: true,
  })
  createEdition(
    @Body() edition: CreateEditionRequestDto,
  ): Promise<CompleteEditionResponseDto> {
    return this.editionService.create(edition);
  }

  @Patch("activate/:id")
  @UseGuards(ZitadelAuthGuard, RolesGuard)
  @Roles(oidcRoles.ADMIN)
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description:
      "Set the given edition to active and desactivate previously activated edition(s)",
    type: CompleteEditionResponseDto,
  })
  setActiveEdition(
    @Param("id", ParseIntPipe) id: number,
  ): Promise<CompleteEditionResponseDto> {
    return this.editionService.setActiveEdition(id);
  }

  @Patch(":id")
  @UseGuards(ZitadelAuthGuard, RolesGuard)
  @Roles(oidcRoles.ADMIN)
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: "Update an existing edition",
    type: CompleteEditionResponseDto,
  })
  @ApiBody({
    type: UpdateEditionRequestDto,
    description:
      "Fields that can be updated: name, start/end dates, start/end registration dates.",
    required: true,
  })
  updateEdition(
    @Param("id", ParseIntPipe) id: number,
    @Body() edition: UpdateEditionRequestDto,
  ): Promise<CompleteEditionResponseDto> {
    return this.editionService.update(id, edition);
  }

  @Delete(":id")
  @UseGuards(ZitadelAuthGuard, RolesGuard)
  @Roles(oidcRoles.ADMIN)
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    description: "Deletes edition with the given id",
  })
  deleteEdition(@Param("id", ParseIntPipe) id: number): Promise<void> {
    return this.editionService.deleteById(id);
  }
}
