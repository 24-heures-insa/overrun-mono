import {
  IsBoolean,
  IsDate,
  IsInt,
  IsNotEmpty,
  IsString,
} from "class-validator";
import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { CompleteEditionResponse } from "@overrun/types";

export class CompleteEditionResponseDto implements CompleteEditionResponse {
  @ApiProperty({
    required: true,
    description: "The id of the edition",
    type: Number,
  })
  @IsInt()
  @IsNotEmpty()
  id: number;

  @ApiProperty({
    required: true,
    description: "The name of the edition",
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    required: true,
    description: "The status of the edition",
    type: Boolean,
  })
  @IsBoolean()
  @IsNotEmpty()
  active: boolean;

  @ApiProperty({
    required: true,
    description: "The start date of the registration period of the edition",
    type: Date,
  })
  @IsDate()
  @IsNotEmpty()
  registrationStartDate: Date;

  @ApiProperty({
    required: true,
    description: "The end date of the registration period of the edition",
    type: Date,
  })
  @IsDate()
  @IsNotEmpty()
  registrationEndDate: Date;

  @ApiProperty({
    required: true,
    description: "The start date of the race period of the edition",
    type: Date,
  })
  @IsDate()
  @IsNotEmpty()
  startDate: Date;

  @ApiProperty({
    required: true,
    description: "The end date of the race period of the edition",
    type: Date,
  })
  @IsDate()
  @IsNotEmpty()
  endDate: Date;

  @ApiPropertyOptional({
    required: true,
    description: "The html description of the fundraiser",
    type: Object,
  })
  fundraiser: { description: string };
}
