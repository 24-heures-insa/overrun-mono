import { IsNotEmpty, IsString, IsDateString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";
import { CreateEditionRequest } from "@overrun/types";

export class CreateEditionRequestDto implements CreateEditionRequest {
  @ApiProperty({
    required: true,
    description: "The name of the edition",
    type: String,
  })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    required: true,
    description: "The start date of the race",
    type: Date,
  })
  @IsDateString() //format attendu: ISO-8601
  @IsNotEmpty()
  startDate: Date;

  @ApiProperty({
    required: true,
    description: "The end date of the race",
    type: Date,
  })
  @IsDateString()
  @IsNotEmpty()
  endDate: Date;

  @ApiProperty({
    required: true,
    description: "The start date for inscriptions",
    type: Date,
  })
  @IsDateString()
  @IsNotEmpty()
  registrationStartDate: Date;

  @ApiProperty({
    required: true,
    description: "The end date for inscriptions",
    type: Date,
  })
  @IsDateString()
  @IsNotEmpty()
  registrationEndDate: Date;
}
