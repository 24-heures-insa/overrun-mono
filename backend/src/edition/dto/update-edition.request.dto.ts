import { IsString, IsDate, IsOptional } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";
import { UpdateEditionRequest } from "@overrun/types";

export class UpdateEditionRequestDto implements UpdateEditionRequest {
  @ApiProperty({
    required: false,
    description: "The name of the edition",
    type: String,
  })
  @IsOptional()
  @IsString()
  name?: string;

  @ApiProperty({
    required: false,
    description: "The start date of the race period of the edition",
    type: Date,
  })
  @IsOptional()
  @IsDate() //format attendu: ISO-8601
  startDate?: Date;

  @ApiProperty({
    required: false,
    description: "The end date of the race period of the edition",
    type: Date,
  })
  @IsOptional()
  @IsDate()
  endDate?: Date;

  @ApiProperty({
    required: false,
    description: "The start date of the registration period of the edition",
    type: Date,
  })
  @IsOptional()
  @IsDate()
  registrationStartDate?: Date;

  @ApiProperty({
    required: false,
    description: "The end date of the registration period of the edition",
    type: Date,
  })
  @IsOptional()
  @IsDate()
  registrationEndDate?: Date;
}
