import { Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma.service";
import { CompleteEditionResponseDto } from "./dto/complete-edition.response.dto";
import { CreateEditionRequestDto } from "./dto/create-edition.request.dto";
import { UpdateEditionRequestDto } from "./dto/update-edition.request.dto";

const selection = {
  id: true,
  name: true,
  active: true,
  startDate: true,
  endDate: true,
  registrationStartDate: true,
  registrationEndDate: true,
  fundraiser: {
    select: {
      description: true,
    },
  },
};

@Injectable()
export class EditionService {
  constructor(private readonly prisma: PrismaService) {}

  async getAll(): Promise<CompleteEditionResponseDto[]> {
    return await this.prisma.edition.findMany({
      orderBy: {
        id: "desc",
      },
      select: selection,
    });
  }

  async getById(id: number): Promise<CompleteEditionResponseDto> {
    return await this.prisma.edition.findUnique({
      where: { id },
      select: selection,
    });
  }

  async getActiveEdition(): Promise<CompleteEditionResponseDto> {
    return await this.prisma.edition.findFirst({
      where: {
        active: true,
      },
      select: selection,
    });
  }

  async setActiveEdition(id: number): Promise<CompleteEditionResponseDto> {
    const [_, edition] = await this.prisma.$transaction([
      this.prisma.edition.updateMany({
        where: {
          active: true,
        },
        data: {
          active: false,
        },
      }),
      this.prisma.edition.update({
        where: { id },
        data: {
          active: true,
        },
        select: selection,
      }),
    ]);
    return edition;
  }

  async create(
    editionCreation: CreateEditionRequestDto,
  ): Promise<CompleteEditionResponseDto> {
    return await this.prisma.edition.create({
      data: {
        ...editionCreation,
        active: false,
      },
      select: selection,
    });
  }

  async update(
    id: number,
    e: UpdateEditionRequestDto,
  ): Promise<CompleteEditionResponseDto> {
    const edition = await this.prisma.edition.findUnique({
      where: { id },
    });
    const data = {
      name: e.name || edition.name,
      startDate: e.startDate || edition.startDate,
      endDate: e.endDate || edition.endDate,
      registrationStartDate:
        e.registrationStartDate || edition.registrationStartDate,
      registrationEndDate: e.registrationEndDate || edition.registrationEndDate,
    };
    return await this.prisma.edition.update({
      where: { id },
      data: data,
      select: selection,
    });
  }

  async deleteById(id: number): Promise<void> {
    await this.prisma.edition.delete({
      where: { id },
    });
  }
}
