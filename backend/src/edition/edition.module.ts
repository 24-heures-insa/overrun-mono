import { Module } from "@nestjs/common";
import { EditionController } from "./edition.controller";
import { EditionService } from "./edition.service";
import { CommonModule } from "src/common/common.module";

@Module({
  imports: [CommonModule],
  controllers: [EditionController],
  providers: [EditionService],
})
export class EditionModule {}
