import { Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma.service";
import { InscriptionStatus } from "@overrun/types";

@Injectable()
export class CommonService {
  constructor(private readonly prisma: PrismaService) {}

  /**
   * @userId : l'utilisateur dont on souhaite récupérer la dernière inscription
   *
   * @return : l'id de l'inscription en cours, ou null s'il n'y en a pas
   * @throws une erreur si il existe plusieurs inscriptions en cours
   *
   * Une inscription en cours d'annulation est considérée comme active
   * car un utilisateur ne doit pas pouvoir en recréer une sans
   * validation de l'annulation par un administrateur.
   */
  async getActiveInscription(userId: number): Promise<number> {
    const inscriptions = await this.prisma.inscription.findMany({
      where: {
        athleteId: userId,
        status: {
          in: [
            InscriptionStatus.PENDING,
            InscriptionStatus.VALIDATED,
            InscriptionStatus.CANCEL_ASKED,
          ],
        },
      },
    });

    if (inscriptions.length === 0) {
      return null;
    }

    if (inscriptions.length > 1) {
      throw new Error("Several inscriptions exist");
    }

    return inscriptions[0].id;
  }
}
