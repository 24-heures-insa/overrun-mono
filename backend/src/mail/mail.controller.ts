import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiTags,
  ApiUnauthorizedResponse,
} from "@nestjs/swagger";
import { Body, Controller, Post, UseGuards } from "@nestjs/common";
import { MailService } from "./mail.service";
import { MailTestRequestDto } from "./dto/mail-test.request.dto";
import { RolesGuard, ZitadelAuthGuard } from "src/authentication/guards";
import { Roles } from "src/authentication/decorators";
import { oidcRoles } from "@overrun/types";

@UseGuards(ZitadelAuthGuard, RolesGuard)
@Roles(oidcRoles.ADMIN)
@ApiBearerAuth()
@ApiTags("mail")
@Controller("mail")
@ApiBadRequestResponse({
  description: "Bad Request",
})
export class MailController {
  constructor(private readonly mailService: MailService) {}

  @ApiBody({
    description: "Route de test pour le service mail",
    type: MailTestRequestDto,
  })
  @ApiUnauthorizedResponse({
    description: "User dont have the right to access this route",
  })
  @Post("mailtest")
  async mailtest(@Body() to: MailTestRequestDto) {
    return this.mailService.mailTest(to);
  }
}
