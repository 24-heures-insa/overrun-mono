import { Injectable } from "@nestjs/common";
import { PrismaService } from "src/prisma.service";
import { CreatePaymentDto } from "./dto/create-payment.request.dto";
import { HelloAssoService } from "./helloasso.service";
import { CheckoutIntentDto } from "./dto/helloasso-checkout.request.dto";
import { CheckoutIntentResponseDto } from "./dto/helloasso-checkout.response.dto";
import { HelloassoUrlDto, PaymentStatus } from "@overrun/types";
import { MIN_TO_MS } from "@overrun/constant";
import { HelloassoNotification } from "./dto/helloasso-confirmation.notification.dto";

@Injectable()
export class PaymentService {
  constructor(
    private readonly prismaService: PrismaService,
    private readonly helloassoService: HelloAssoService,
  ) {}

  async countDonationAmountByEdition(
    editionId: number,
  ): Promise<{ result: number }> {
    const where = {
      inscription: {
        race: {
          category: {
            edition: {},
          },
        },
      },
    };

    if (editionId) {
      where.inscription.race.category.edition = { id: editionId };
    } else {
      where.inscription.race.category.edition = { active: true };
    }

    const result = await this.prismaService.payment.aggregate({
      where,
      _sum: {
        donationAmount: true,
      },
    });
    return {
      result: result._sum.donationAmount ?? 0,
    };
  }

  async createNewCheckoutIntent(
    dto: CreatePaymentDto,
  ): Promise<HelloassoUrlDto> {
    const checkoutIntentDto: CheckoutIntentDto = {
      totalAmount: dto.raceAmount + dto.donationAmount,
      initialAmount: dto.raceAmount + dto.donationAmount,
      itemName: dto.paymentLabel,
      backUrl: HelloAssoService.GO_BACK_URL,
      errorUrl: HelloAssoService.ERROR_URL,
      returnUrl: HelloAssoService.RETURN_URL,
      containsDonation: !!dto.donationAmount,
      metadata: {
        inscriptionId: dto.inscriptionId,
      },
    };

    const checkoutResponse: CheckoutIntentResponseDto =
      await this.helloassoService.createCheckoutIntent(checkoutIntentDto);

    const expirationDate = new Date(
      new Date().getTime() +
        HelloAssoService.CHECKOUT_EXPIRATION_DELAY * MIN_TO_MS,
    );

    await this.prismaService.payment.upsert({
      where: {
        inscriptionId: dto.inscriptionId,
      },
      update: {
        raceAmount: dto.raceAmount,
        donationAmount: dto.donationAmount,
        totalAmount: dto.raceAmount + dto.donationAmount,
        helloassoCheckoutIntentUrl: checkoutResponse.redirectUrl,
        helloassoCheckoutIntentId: checkoutResponse.id,
        status: PaymentStatus.PENDING,
        helloassoCheckoutExpiresAt: expirationDate,
      },
      create: {
        raceAmount: dto.raceAmount,
        donationAmount: dto.donationAmount,
        totalAmount: dto.raceAmount + dto.donationAmount,
        helloassoCheckoutIntentUrl: checkoutResponse.redirectUrl,
        helloassoCheckoutIntentId: checkoutResponse.id,
        status: PaymentStatus.PENDING,
        helloassoCheckoutExpiresAt: expirationDate,
        inscriptionId: dto.inscriptionId,
      },
    });

    return { checkoutUrl: checkoutResponse.redirectUrl };
  }

  async handlePaymentNotification({ data, metadata }: HelloassoNotification) {
    let nextState: PaymentStatus;
    switch (data.state) {
      case "Authorized":
        nextState = PaymentStatus.VALIDATED;
        break;
      case "Refunded":
        nextState = PaymentStatus.REFUND;
        break;
      default:
        nextState = PaymentStatus.REFUSED;
    }
    await this.prismaService.payment.update({
      where: {
        inscriptionId: metadata.inscriptionId,
      },
      data: {
        helloassoPaymentReceiptUrl: data.paymentReceiptUrl,
        status: nextState,
      },
    });
  }
}
