import {
  ApiBadRequestResponse,
  ApiBody,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from "@nestjs/swagger";
import {
  Body,
  Controller,
  Post,
  Logger,
  Get,
  Query,
  ParseIntPipe,
} from "@nestjs/common";
import { PaymentService } from "./payment.service";
import { CreatePaymentDto } from "./dto/create-payment.request.dto";
import { HelloassoNotification } from "./dto/helloasso-confirmation.notification.dto";
import { HelloassoUrlDto, oidcRoles } from "@overrun/types";
import { Roles } from "src/authentication/decorators";

@ApiTags("payments")
@Controller("payments")
@ApiBadRequestResponse({
  description: "Bad Request",
})
export class PaymentController {
  private readonly logger = new Logger(PaymentController.name);
  constructor(private readonly paymentService: PaymentService) {}

  @Post("checkout")
  @ApiResponse({
    status: 200,
    description: "Create a checkout intent with Helloasso",
    type: HelloassoUrlDto,
  })
  @ApiBody({
    type: CreatePaymentDto,
    description: "Information about the payment",
    required: true,
  })
  createCheckoutIntent(
    @Body() body: CreatePaymentDto,
  ): Promise<HelloassoUrlDto> {
    return this.paymentService.createNewCheckoutIntent(body);
  }

  @Get("donation")
  @Roles(oidcRoles.ADMIN)
  @ApiQuery({
    name: "editionId",
    type: Number,
    description:
      "Optional. If not specified, returns the donation amount for the currently active edition.",
    required: false,
  })
  @ApiResponse({
    status: 200,
    description:
      "Get the sum of all donation to the fundraiser for a given edition",
  })
  getDonationAmountByEdition(
    @Query("editionId", ParseIntPipe) editionId?: number,
  ): Promise<{ result: number }> {
    return this.paymentService.countDonationAmountByEdition(editionId);
  }

  @Post("notifications")
  @ApiBody({
    type: HelloassoNotification,
    description:
      "Dto as described here : https://dev.helloasso.com/docs/notification-exemple",
    required: true,
  })
  handleHelloassoNotification(@Body() body: HelloassoNotification) {
    switch (body.eventType) {
      case "Payment":
        return this.paymentService.handlePaymentNotification(body);
      case "Order":
        this.logger.log(
          `Confirmation de commande pour l'inscription #${body.metadata?.inscriptionId} checkout intent #${body.data.checkoutIntentId}`,
        );
        break;
      case "Organization":
      case "Form":
        this.logger.warn(`Notification HelloAsso non gérée ${body.eventType}`);
        break;
    }
  }
}
