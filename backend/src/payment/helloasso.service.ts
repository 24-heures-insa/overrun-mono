import { Injectable, Logger } from "@nestjs/common";
import { CheckoutIntentDto } from "./dto/helloasso-checkout.request.dto";
import { HelloassoError, TokenResponseDto } from "./dto/helloasso-token.dto";
import { DAY_TO_MS, SEC_TO_MS } from "@overrun/constant";
import { CheckoutIntentResponseDto } from "./dto/helloasso-checkout.response.dto";

@Injectable()
export class HelloAssoService {
  constructor() {}
  private accessToken: string;
  private refreshToken: string;
  private accessExpiration: Date;
  private refreshExpiration: Date;
  private readonly logger = new Logger(HelloAssoService.name);

  private static HELLOASSO_TOKEN_REFRESH_DELAY = 30; //jours
  private static HELLOASSO_URL = process.env.HELLOASSO_URL;

  public static CHECKOUT_EXPIRATION_DELAY = 15; //minutes
  public static GO_BACK_URL = `https://${process.env.OVERRUN_DOMAIN}/dashboard`;
  public static ERROR_URL = `https://${process.env.OVERRUN_DOMAIN}/dashboard?type=error`;
  public static RETURN_URL = `https://${process.env.OVERRUN_DOMAIN}/dashboard?type=success`;

  private async getOrRefreshAccessToken() {
    //Première initialisation ou refresh token expiré
    // => demande d'un nouveau refresh et access token
    if (
      !this.accessToken ||
      !this.refreshToken ||
      this.refreshExpiration < new Date()
    ) {
      await this.fetchAccessAndRefreshToken();
      return this.accessToken;
    }

    //Access token expiré
    if (this.accessExpiration < new Date()) {
      await this.refreshAccessToken();
      return this.accessToken;
    }

    //Access token valide
    return this.accessToken;
  }

  private async fetchAccessAndRefreshToken() {
    const dto = {
      client_id: process.env.HELLOASSO_CLIENT_ID,
      client_secret: process.env.HELLOASSO_CLIENT_SECRET,
      grant_type: "client_credentials",
    };
    const options = {
      method: "POST",
      headers: {
        accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body: new URLSearchParams(dto),
    };
    const response = await fetch(
      `${HelloAssoService.HELLOASSO_URL}/oauth2/token`,
      options,
    );

    await this.updateTokens(response);
  }

  private async refreshAccessToken() {
    const dto = {
      client_id: process.env.HELLOASSO_CLIENT_ID,
      refresh_token: this.refreshToken,
      grant_type: "refresh_token",
    };
    const options = {
      method: "POST",
      headers: {
        accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body: new URLSearchParams(dto),
    };
    const response = await fetch(
      `${HelloAssoService.HELLOASSO_URL}/oauth2/token`,
      options,
    );

    await this.updateTokens(response);
  }

  private async updateTokens(response: Response) {
    if (response.status < 300) {
      //success
      const json = (await response.json()) as TokenResponseDto;

      this.accessToken = json.access_token;
      this.accessExpiration = new Date(
        new Date().getTime() + json.expires_in * SEC_TO_MS,
      );

      this.refreshToken = json.refresh_token;
      this.refreshExpiration = new Date(
        new Date().getTime() +
          HelloAssoService.HELLOASSO_TOKEN_REFRESH_DELAY * DAY_TO_MS,
      );

      return this.accessToken;
    }

    //error
    const json = (await response.json()) as HelloassoError;
    const errors = json.errors
      .map(({ code, message }) => `${code} => ${message}`)
      .join(", ");
    this.logger.error(`Erreur ${response.status} Helloasso : ${errors}`);
    throw Error(errors);
  }

  public async createCheckoutIntent(
    dto: CheckoutIntentDto,
  ): Promise<CheckoutIntentResponseDto> {
    await this.getOrRefreshAccessToken();

    const options: RequestInit = {
      method: "POST",
      headers: {
        accept: "application/json",
        "content-type": "application/*+json",
        authorization: `Bearer ${this.accessToken}`,
      },
      body: JSON.stringify(dto),
    };
    const response = await fetch(
      `${HelloAssoService.HELLOASSO_URL}/v5/organizations/${process.env.HELLOASSO_SLUG}/checkout-intents`,
      options,
    );

    if (response.status < 300) {
      //success
      return (await response.json()) as CheckoutIntentResponseDto;
    }

    //error
    const json = (await response.json()) as HelloassoError;
    const errors = json.errors
      .map(({ code, message }) => `${code} => ${message}`)
      .join(", ");
    this.logger.error(`Erreur ${response.status} Helloasso : ${errors}`);
    throw Error(errors);
  }
}
