import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsInt, IsOptional, IsString } from "class-validator";

export class CreatePaymentDto {
  @IsInt()
  @ApiProperty({
    required: true,
    type: Number,
  })
  inscriptionId: number;

  @IsInt()
  @ApiProperty({
    required: true,
    type: Number,
  })
  raceAmount: number;

  @IsInt()
  @IsOptional()
  @ApiPropertyOptional({
    type: Number,
  })
  donationAmount?: number;

  @IsString()
  @ApiPropertyOptional({
    type: String,
  })
  paymentLabel: string;
}
