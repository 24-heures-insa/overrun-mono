export class PaymentLiteResponseDto {
  id: number;
  raceAmount: number;
  donationAmount: number;
  totalAmount: number;
  status: PaymentStatusDto;
  refundAsked: boolean;
  helloassoCheckoutExpiresAt?: Date;
  helloassoCheckoutIntentUrl?: string;
  helloassoPaymentReceiptUrl?: string;
}

export const PaymentStatus: {
  NOT_STARTED: "NOT_STARTED";
  PENDING: "PENDING";
  VALIDATED: "VALIDATED";
  REFUSED: "REFUSED";
  REFUND: "REFUND";
  REFUNDING: "REFUNDING";
} = {
  NOT_STARTED: "NOT_STARTED",
  PENDING: "PENDING",
  VALIDATED: "VALIDATED",
  REFUSED: "REFUSED",
  REFUND: "REFUND",
  REFUNDING: "REFUNDING",
};

export type PaymentStatusDto =
  (typeof PaymentStatus)[keyof typeof PaymentStatus];
