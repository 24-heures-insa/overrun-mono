//https://dev.helloasso.com/docs/getting-started > S'authentifier pour utiliser l'API

//24-heures-de-l-insa

export class RefreshTokenRequestDto {
  //tous les champs sont requis
  client_id: string;
  grant_type: "refresh_token";
  refresh_token: string;
}

export class TokenResponseDto {
  access_token: string;
  refresh_token: string;
  token_type: "bearer";
  expires_in: number;
}

export class HelloassoError {
  errors: { code: string; message }[];
}
