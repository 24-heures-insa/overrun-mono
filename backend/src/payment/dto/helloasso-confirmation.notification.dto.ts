import { IsDefined, IsOptional } from "class-validator";

export class HelloassoNotification {
  @IsDefined()
  data: {
    paymentReceiptUrl: string;
    state: HelloassoPaymentStatus;
    id: number;
    amount: number;
    checkoutIntentId?: number;
    order?: {
      id: number;
    };
  };
  @IsDefined()
  eventType: HelloassoEventType;

  @IsOptional()
  metadata: {
    inscriptionId: number;
  };
}

export type HelloassoPaymentStatus =
  | "Pending"
  | "Authorized"
  | "Refused"
  | "Unknow"
  | "Registered"
  | "Refunded"
  | "Refunding"
  | "Contested";

export type HelloassoEventType = "Form" | "Order" | "Payment" | "Organization";
