//https://dev.helloasso.com/reference/post_organizations-organizationslug-checkout-intents
export class CheckoutIntentResponseDto {
  id: number;
  redirectUrl: string;
}
