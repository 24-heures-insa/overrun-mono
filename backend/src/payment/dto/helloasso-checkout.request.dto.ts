import { RawDateString, RawDatetimeString } from "@overrun/types";

//https://dev.helloasso.com/reference/post_organizations-organizationslug-checkout-intents
export class CheckoutIntentDto {
  //required
  totalAmount: number;
  initialAmount: number;
  itemName: string;
  backUrl: string;
  errorUrl: string;
  returnUrl: string;
  containsDonation: boolean;

  //not required
  terms?: TermDto[];
  payer?: {
    /*
      Pour le nom et prénom, choses interdites :
        3 caractères répétitifs, Un chiffre, Un seul caractère,  Aucune voyelle
        Les valeurs : "firstname", "lastname", "unknown", "first_name", "last_name", "anonyme", “user", "admin", "name", “nom", "prénom", "test”
        Caractères spéciaux, à l’exception des lettres "e", "a" et "u" accentuées, "‘", "-", "ç"
        Caractères n’appartenant pas à l’alphabet latin

        Nom et prénom doivent être distincts
    */
    firstName?: string;
    lastName?: string;
    email?: string; //email valide nécessaire
    dateOfBirth?: RawDateString; //paiement impossible si mineur
    address?: string;
    city?: string;
    zipCode?: string;
    country?: string; //Code 3 lettres
    companyName?: string;
  };
  metadata: {
    inscriptionId: number;
  };
}

//pour les paiements avec échéances
class TermDto {
  //required
  amount: number;
  date: RawDatetimeString;
}
