import { Body, Controller, Get, Post, Query, UseGuards } from "@nestjs/common";
import {
  ApiTags,
  ApiBadRequestResponse,
  ApiResponse,
  ApiBody,
  ApiQuery,
  ApiBearerAuth,
} from "@nestjs/swagger";
import { TeamService } from "./team.service";
import { TeamCreatedDto } from "./dto/team-creation.response.dto";
import { TeamCreationDto } from "./dto/team-creation.request.dto";
import { CompleteTeamDto } from "./dto/complete-team.response.dto";
import { RolesGuard, ZitadelAuthGuard } from "src/authentication/guards";
import { Roles } from "src/authentication/decorators";
import { oidcRoles } from "@overrun/types";

@UseGuards(ZitadelAuthGuard, RolesGuard)
@ApiBearerAuth()
@ApiTags("teams")
@Controller("teams")
@ApiBadRequestResponse({
  description: "Bad Request",
})
export class TeamController {
  constructor(private readonly teamService: TeamService) {}

  @Post()
  @Roles(oidcRoles.ADMIN, oidcRoles.ATHLETE)
  @ApiBody({
    type: TeamCreationDto,
    required: true,
  })
  @ApiResponse({
    status: 200,
    description: "Create a new team",
  })
  createTeam(@Body() data: TeamCreationDto): Promise<TeamCreatedDto> {
    return this.teamService.createTeam(data);
  }

  @Get()
  @Roles(oidcRoles.ADMIN, oidcRoles.ATHLETE)
  @ApiQuery({
    name: "editionId",
    type: Number,
    required: true,
  })
  @ApiResponse({
    status: 200,
    description:
      "Get all teams with at least one available spot from a given edition",
    type: CompleteTeamDto,
  })
  getTeamsForInscription(
    @Query("editionId") editionId: number,
  ): Promise<CompleteTeamDto[]> {
    return this.teamService.getTeamsForInscription(editionId);
  }
}
