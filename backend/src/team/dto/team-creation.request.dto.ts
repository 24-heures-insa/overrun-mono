import { ApiProperty } from "@nestjs/swagger";
import { IsNumber, IsString } from "class-validator";

export class TeamCreationDto {
  @ApiProperty({
    required: true,
    type: Number,
  })
  @IsNumber()
  raceId: number;

  @ApiProperty({
    required: true,
    type: String,
  })
  @IsString()
  name: string;
}
