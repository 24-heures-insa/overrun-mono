export class TeammateResponseDto {
  athleteId: number;
  firstName: string;
  lastName: string;
  isAdmin: boolean;
  payment: boolean;
  certificate: boolean;
  inscription: boolean;
}
