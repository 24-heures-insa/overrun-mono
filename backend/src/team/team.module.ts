import { Module } from "@nestjs/common";
import { TeamController } from "./team.controller";
import { TeamService } from "./team.service";
import { CommonModule } from "src/common/common.module";

@Module({
  imports: [CommonModule],
  controllers: [TeamController],
  providers: [TeamService],
})
export class TeamModule {}
