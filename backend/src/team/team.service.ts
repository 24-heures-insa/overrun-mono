import { Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma.service";
import { TeamCreationDto } from "./dto/team-creation.request.dto";
import { TeamCreatedDto } from "./dto/team-creation.response.dto";
import { CompleteTeamDto } from "./dto/complete-team.response.dto";
import { InscriptionStatus } from "@overrun/types";

@Injectable()
export class TeamService {
  constructor(private readonly prisma: PrismaService) {}

  async getTeamsForInscription(editionId: number): Promise<CompleteTeamDto[]> {
    /*
      select t.* from "Team" t
        left join "Inscription" i on t.id = i."teamId"
        join "Race" r on r.id = t."raceId"
        join "Category" c on c.id = r."categoryId"
      where c."editionId" = 1
      group by (t.id, c."maxTeamMembers")
      having c."maxTeamMembers" > count(i.id) and count(i.id) > 0;
    */

    const teams = (
      await this.prisma.team.findMany({
        where: {
          race: {
            category: {
              editionId: editionId,
            },
          },
        },
        include: {
          race: {
            select: {
              id: true,
              disciplineDurations: { select: { disciplineId: true } },
              category: { select: { id: true, maxTeamMembers: true } },
            },
          },
          _count: {
            select: {
              members: {
                where: {
                  status: { not: InscriptionStatus.CANCELLED },
                },
              },
            },
          },
        },
      })
    )
      .filter(
        (team) =>
          team._count.members > 0 &&
          team._count.members < team.race.category.maxTeamMembers,
      )
      .map((team) => this.toCompleteTeam(team));

    return teams;
  }

  async createTeam(team: TeamCreationDto): Promise<TeamCreatedDto> {
    //création du token pour rejoindre l'équipe
    const token = "token";

    const created = await this.prisma.team.create({
      data: {
        raceId: team.raceId,
        name: team.name,
        password: token,
      },
    });

    return {
      idTeam: created.id,
      name: created.name,
      token: created.password,
    };
  }

  //sûrement un meilleur moyen de faire ça avec
  // nestjs + prisma
  private toCompleteTeam(
    team: {
      race: {
        id: number;
        disciplineDurations: { disciplineId: number }[];
        category: { id: number; maxTeamMembers: number };
      };
      _count: { members: number };
    } & { id: number; name: string; password: string; raceId: number },
  ) {
    return {
      id: team.id,
      name: team.name,
      freeSpots: team.race.category.maxTeamMembers - team._count.members,
      totalSpots: team.race.category.maxTeamMembers,
      raceId: team.race.id,
      categoryId: team.race.category.id,
      disciplineIds: team.race.disciplineDurations.map((dd) => dd.disciplineId),
    };
  }
}
