import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { PrismaModule } from "./prisma.module";
import { CategoryModule } from "./category/category.module";
import { DisciplineModule } from "./discipline/discipline.module";
import { EditionModule } from "./edition/edition.module";
import { MailerModule } from "@nestjs-modules/mailer";
import { EjsAdapter } from "@nestjs-modules/mailer/dist/adapters/ejs.adapter";
import { MailModule } from "./mail/mail.module";
import { UserModule } from "./user/user.module";
import { InscriptionModule } from "./inscription/inscription.module";
import { TeamModule } from "./team/team.module";
import { RaceModule } from "./race/race.module";
import { FileModule } from "./file/file.module";
import { VaModule } from "./va/va.module";
import { CommonModule } from "./common/common.module";
import { PassportModule } from "@nestjs/passport";
import { ZitadelAuthModule } from "./authentication/zitadel-auth.module";
import { PaymentModule } from "./payment/payment.module";
import { FundraiserModule } from "./fundraiser/fundraiser.module";

@Module({
  imports: [
    /**Modules extérieurs */
    MailerModule.forRoot({
      transport: {
        host: process.env.SMTP_HOST,
        port: process.env.SMTP_PORT,
        auth: {
          user: process.env.GMAIL_USER,
          pass: process.env.GMAIL_PASS,
        },
      },
      defaults: {
        from: `"OverRun" <${process.env.GMAIL_USER}>`,
      },
      template: {
        dir: __dirname + "/mail/templates",
        adapter: new EjsAdapter(),
        options: {
          strict: false,
        },
      },
    }),
    /**Modules partagés*/
    CommonModule,
    PrismaModule,
    PassportModule,
    ZitadelAuthModule.forRoot({
      authority: "https://zitadel.24heures.org",
      authorization: {
        type: "jwt-profile",
        profile: {
          type: "application",
          keyId: process.env.ZITADEL_KEY_ID,
          key: process.env.ZITADEL_KEY,
          appId: process.env.ZITADEL_APP_ID,
          clientId: process.env.ZITADEL_CLIENT_ID,
        },
      },
    }),
    /**Modules de l'application */
    CategoryModule,
    DisciplineModule,
    EditionModule,
    FileModule,
    FundraiserModule,
    InscriptionModule,
    MailModule,
    PaymentModule,
    RaceModule,
    TeamModule,
    UserModule,
    VaModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
