import { Module } from "@nestjs/common";
import { FundraiserController } from "./fundraiser.controller";
import { FundraiserService } from "./fundraiser.service";
import { CommonModule } from "src/common/common.module";
import { FileModule } from "src/file/file.module";

@Module({
  imports: [CommonModule, FileModule],
  controllers: [FundraiserController],
  providers: [FundraiserService],
})
export class FundraiserModule {}
