import { IsString, IsNumber, IsArray } from "class-validator";
import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { UpsertFundraiserDto } from "@overrun/types";

export class FundraiserDtoRequest implements UpsertFundraiserDto {
  @ApiPropertyOptional({
    type: String,
  })
  @IsString()
  name?: string;

  @ApiPropertyOptional({
    type: String,
  })
  @IsString()
  description?: string;

  @ApiProperty({
    type: Number,
    required: true,
  })
  @IsNumber()
  editionId: number;

  @ApiPropertyOptional({
    type: String,
    isArray: true,
  })
  @IsArray()
  images?: string[];
}
