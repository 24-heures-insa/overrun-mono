import { ApiProperty } from "@nestjs/swagger";
import { FundraiserDto } from "@overrun/types";

export class FundraiserDtoResponse implements FundraiserDto {
  @ApiProperty({
    type: String,
  })
  id: number;

  @ApiProperty({
    type: String,
  })
  name: string;

  @ApiProperty({
    type: String,
  })
  description: string;

  @ApiProperty({
    type: Number,
  })
  editionId: number;
}
