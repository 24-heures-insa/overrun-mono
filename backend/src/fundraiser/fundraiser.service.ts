import { Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma.service";
import { FundraiserDtoResponse } from "./dto/upsert-fundraiser.response.dto";
import { FundraiserDtoRequest } from "./dto/upsert-fundraiser.request.dto";
import { FileService } from "src/file/file.service";

@Injectable()
export class FundraiserService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly fileService: FileService,
  ) {}

  async getAll(): Promise<FundraiserDtoResponse[]> {
    return await this.prisma.fundraiser.findMany({
      orderBy: {
        id: "desc",
      },
    });
  }

  async getById(id: number): Promise<FundraiserDtoResponse> {
    return await this.prisma.fundraiser.findUnique({
      where: { id },
    });
  }

  async getFromActiveEdition(): Promise<FundraiserDtoResponse> {
    return await this.prisma.fundraiser.findFirst({
      where: {
        edition: {
          active: true,
        },
      },
    });
  }

  async getFromEditionId(editionId: number): Promise<FundraiserDtoResponse> {
    return await this.prisma.fundraiser.findUnique({
      where: { editionId },
    });
  }

  async edit(dto: FundraiserDtoRequest): Promise<FundraiserDtoResponse> {
    const fundraiser = await this.prisma.fundraiser.findUnique({
      where: {
        editionId: dto.editionId,
      },
      select: {
        images: true,
      },
    });

    // Création de la course caritative
    if (!fundraiser) {
      return await this.prisma.fundraiser.create({
        data: {
          description: dto.description ?? "",
          name: dto.name ?? "",
          images: dto.images ?? [],
          editionId: dto.editionId,
        },
      });
    }

    //Gestion des images
    const toDelete = fundraiser.images.filter(
      (img) => !dto.images?.includes(img),
    );
    toDelete.forEach((td) =>
      this.fileService.deleteFile(td, FileService.PATH_RICH_EDITOR),
    );
    return await this.prisma.fundraiser.update({
      data: dto,
      where: { editionId: dto.editionId },
    });
  }
}
