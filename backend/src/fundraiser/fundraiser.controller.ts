import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  UseGuards,
} from "@nestjs/common";
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiParam,
  ApiResponse,
  ApiTags,
} from "@nestjs/swagger";
import { FundraiserService } from "./fundraiser.service";
import { oidcRoles } from "@overrun/types";
import { Roles } from "src/authentication/decorators";
import { ZitadelAuthGuard, RolesGuard } from "src/authentication/guards";
import { FundraiserDtoResponse } from "./dto/upsert-fundraiser.response.dto";
import { FundraiserDtoRequest } from "./dto/upsert-fundraiser.request.dto";

@ApiTags("fundraisers")
@Controller("fundraisers")
@UseGuards(ZitadelAuthGuard, RolesGuard)
@ApiBearerAuth()
@ApiBadRequestResponse({
  description: "Bad Request",
})
export class FundraiserController {
  constructor(private readonly fundraiserService: FundraiserService) {}

  @Get()
  @Roles(oidcRoles.ADMIN)
  @ApiResponse({
    status: 200,
    description: "Get all fundraisers information",
    type: FundraiserDtoResponse,
  })
  getAll() {
    return this.fundraiserService.getAll();
  }

  @Get("active")
  @Roles(oidcRoles.ADMIN, oidcRoles.ATHLETE)
  @ApiResponse({
    status: 200,
    description: "Get current fundraiser information",
    type: FundraiserDtoResponse,
  })
  getFromActiveEdition() {
    return this.fundraiserService.getFromActiveEdition();
  }

  @Get(":id")
  @Roles(oidcRoles.ADMIN)
  @ApiResponse({
    status: 200,
    description: "Get fundraiser information by id",
    type: FundraiserDtoResponse,
  })
  @ApiParam({
    name: "id",
    type: Number,
  })
  getById(@Param("id", ParseIntPipe) id: number) {
    return this.fundraiserService.getById(id);
  }

  @Get("edition/:id")
  @Roles(oidcRoles.ADMIN)
  @ApiResponse({
    status: 200,
    description: "Get fundraiser information by edition id",
    type: FundraiserDtoResponse,
  })
  @ApiParam({
    name: "id",
    type: Number,
    description:
      "Optional. If specified, only the edition fundraiser is returned.",
    required: false,
  })
  getFromEditionId(@Param("id", ParseIntPipe) editionId: number) {
    return this.fundraiserService.getFromEditionId(editionId);
  }

  @Post()
  @Roles(oidcRoles.ADMIN)
  @ApiResponse({
    status: 200,
    description: "Create or edit and returns a fundraiser",
    type: FundraiserDtoResponse,
  })
  @ApiBody({
    required: true,
    description:
      "The fundraiser that will be create or updated. EditionId is required",
    type: FundraiserDtoRequest,
  })
  edit(@Body() dto: FundraiserDtoRequest) {
    return this.fundraiserService.edit(dto);
  }
}
