import { Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma.service";
import { RaceLiteResponseDto } from "./dto/race-lite.response.dto";

@Injectable()
export class RaceService {
  constructor(private readonly prisma: PrismaService) {}

  async getByEditionId(editionId: number): Promise<RaceLiteResponseDto[]> {
    return await this.prisma.race.findMany({
      where: {
        category: {
          editionId: editionId,
        },
      },
      include: {
        category: true,
        disciplineDurations: true,
      },
      orderBy: {
        id: "asc",
      },
    });
  }

  async getAll(): Promise<RaceLiteResponseDto[]> {
    return await this.prisma.race.findMany({
      orderBy: {
        id: "asc",
      },
    });
  }
}
