import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from "@nestjs/swagger";
import { Controller, Get, Query, UseGuards } from "@nestjs/common";
import { RaceService } from "./race.service";
import { RaceLiteResponseDto } from "./dto/race-lite.response.dto";
import { RolesGuard, ZitadelAuthGuard } from "src/authentication/guards";
import { Roles } from "src/authentication/decorators";
import { oidcRoles } from "@overrun/types";

@UseGuards(ZitadelAuthGuard, RolesGuard)
@ApiTags("races")
@ApiBearerAuth()
@Controller("races")
@ApiBadRequestResponse({
  description: "Bad Request",
})
export class RaceController {
  constructor(private readonly raceService: RaceService) {}

  @Get()
  @Roles(oidcRoles.ADMIN, oidcRoles.ATHLETE)
  @ApiQuery({
    name: "editionId",
    type: Number,
    description: "Optional. If specified, only the edition races are returned.",
    required: false,
  })
  @ApiResponse({
    status: 200,
    description: "Get all races from a given edition. If null, get all races",
    type: RaceLiteResponseDto,
  })
  getRaces(
    @Query("editionId") editionId?: number,
  ): Promise<RaceLiteResponseDto[]> {
    return editionId
      ? this.raceService.getByEditionId(editionId)
      : this.raceService.getAll();
  }
}
