import { LiteCategoryResponseDto } from "src/category/dto/lite-category.response.dto";
import { DisciplineDurationDto } from "src/discipline/dto/disciplineDuration.response.dto";

export class RaceLiteResponseDto {
  id: number;
  name: string;
  registrationPrice?: number;
  vaRegistrationPrice?: number;
  disciplines?: DisciplineDurationDto[];
  category?: LiteCategoryResponseDto;
}
