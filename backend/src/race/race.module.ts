import { Module } from "@nestjs/common";
import { RaceController } from "./race.controller";
import { RaceService } from "./race.service";
import { CommonModule } from "src/common/common.module";

@Module({
  imports: [CommonModule],
  controllers: [RaceController],
  providers: [RaceService],
})
export class RaceModule {}
