import { Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma.service";
import {
  FileDto,
  Filetype,
  MyInfoResponseDto,
} from "./dto/my-info.response.dto";
import { UpdateUserInfoDTO } from "./dto/update-user-info.resquest.dto";
import { CommonService } from "src/common/common.service";

@Injectable()
export class UserService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly commonService: CommonService,
  ) {}

  async updateMyInfo(userInfo: UpdateUserInfoDTO) {
    const userId = 1;
    await this.prisma.user.update({
      where: {
        id: userId,
      },
      data: userInfo,
    });
  }

  async getMyInfo(): Promise<MyInfoResponseDto> {
    //TODO to remove
    const userId = 1;

    const userInfoSelection = {
      id: true,
      email: true,
      username: true,
      firstName: true,
      lastName: true,
      address: true,
      zipCode: true,
      city: true,
      country: true,
      phoneNumber: true,
      dateOfBirth: true,
      gender: true,
    };

    const paymentsInfoSelection = {
      id: true,
      helloassoPaymentReceiptUrl: true,
      inscription: {
        select: {
          race: {
            select: {
              id: true,
              name: true,
              category: {
                select: {
                  edition: {
                    select: {
                      id: true,
                      name: true,
                    },
                  },
                },
              },
            },
          },
        },
      },
    };

    const certificateInfoSelection = {
      id: true,
      filename: true,
      inscription: {
        select: {
          race: {
            select: {
              id: true,
              name: true,
              category: {
                select: {
                  edition: {
                    select: {
                      id: true,
                      name: true,
                    },
                  },
                },
              },
            },
          },
        },
      },
    };

    const teammatesInfoSelection = {
      athlete: {
        select: {
          id: true,
          firstName: true,
          lastName: true,
        },
      },
      teamAdmin: true,
      certificate: {
        select: {
          id: true,
          status: true,
        },
      },
      payment: {
        select: {
          id: true,
          status: true,
        },
      },
      status: true,
    };

    const inscriptionInfoSelection = {
      id: true,
      status: true,
      racingbib: true,
      va: {
        select: {
          id: true,
          validated: true,
        },
      },
      team: {
        select: {
          id: true,
          name: true,
        },
      },
      teamAdmin: true,
      race: {
        select: {
          id: true,
          name: true,
          registrationPrice: true,
          vaRegistrationPrice: true,
          category: {
            select: {
              id: true,
              name: true,
              maxTeamMembers: true,
              minTeamMembers: true,
              description: true,
            },
          },
          disciplineDurations: {
            select: {
              id: true,
              duration: true,
              discipline: {
                select: {
                  name: true,
                },
              },
            },
          },
        },
      },
      payment: {
        select: {
          id: true,
          raceAmount: true,
          donationAmount: true,
          totalAmount: true,
          status: true,
          refundAsked: true,
          helloassoCheckoutExpiresAt: true,
          helloassoCheckoutIntentUrl: true,
          helloassoPaymentReceiptUrl: true,
        },
      },
      certificate: {
        select: {
          id: true,
          filename: true,
          status: true,
        },
      },
    };

    const userInfo = await this.prisma.user.findUnique({
      select: userInfoSelection,
      where: {
        id: userId,
      },
    });

    const certificatesInfo = await this.prisma.certificate.findMany({
      where: {
        inscription: {
          athlete: {
            id: userId,
          },
        },
      },
      select: certificateInfoSelection,
    });

    const filesInfo: FileDto[] = certificatesInfo.map((certificate) => {
      return {
        parentId: certificate.id,
        edition: certificate.inscription.race.category.edition,
        filename: certificate.filename,
        filetype: Filetype.CERTIFICATE,
        race: {
          id: certificate.inscription.race.id,
          name: certificate.inscription.race.name,
        },
      };
    });

    const paymentsInfo = await this.prisma.payment.findMany({
      where: {
        inscription: {
          athlete: {
            id: userId,
          },
        },
      },
      select: paymentsInfoSelection,
    });

    filesInfo.push(
      ...paymentsInfo
        .filter((payment) => payment.helloassoPaymentReceiptUrl)
        .map((payment) => {
          return {
            parentId: payment.id,
            edition: payment.inscription.race.category.edition,
            filename: `Attestation de paiment ${payment.id}`,
            url: payment.helloassoPaymentReceiptUrl,
            filetype: Filetype.PAYMENT,
            race: {
              id: payment.inscription.race.id,
              name: payment.inscription.race.name,
            },
          };
        }),
    );

    const inscriptionId = await this.commonService.getActiveInscription(userId);

    if (!inscriptionId) {
      return {
        ...userInfo,
        currentInscription: undefined,
        files: filesInfo,
      };
    }

    const inscriptionInfo = await this.prisma.inscription.findUnique({
      where: {
        id: inscriptionId,
      },
      select: inscriptionInfoSelection,
    });

    if (inscriptionInfo.team) {
      const teamAdmins = await this.prisma.team.findUnique({
        where: {
          id: inscriptionInfo.team.id,
        },
        select: {
          admins: {
            select: {
              id: true,
            },
          },
        },
      });

      const teammateInfo = (
        await this.prisma.inscription.findMany({
          where: {
            team: {
              id: inscriptionInfo.team.id,
            },
            NOT: {
              status: "CANCELLED",
            },
          },
          select: teammatesInfoSelection,
        })
      ).map((inscription) => {
        return {
          athleteId: inscription.athlete.id,
          firstName: inscription.athlete.firstName,
          lastName: inscription.athlete.lastName,
          isAdmin:
            teamAdmins.admins.filter(
              (admin) => inscription.athlete.id === admin.id,
            ).length > 0,
          payment: inscription.payment?.status === "VALIDATED",
          inscription: inscription.status === "VALIDATED",
          certificate: inscription.certificate?.status === "VALIDATED",
        };
      });

      //todo attention, tu ne renvoies pas les mêmes infos qu'ensuite
      return {
        ...userInfo,
        currentInscription: {
          ...inscriptionInfo,
          va: inscriptionInfo.va?.validated,
          teamName: inscriptionInfo.team.name,
          teamMembers: teammateInfo,
          isAdmin:
            teamAdmins.admins.filter((admin) => userId === admin.id).length > 0,
        },
        files: filesInfo,
      };
    }

    return {
      ...userInfo,
      currentInscription: {
        ...inscriptionInfo,
        va: inscriptionInfo.va ? inscriptionInfo.va.validated : null,
        race: {
          id: inscriptionInfo.race.id,
          name: inscriptionInfo.race.name,
          registrationPrice: inscriptionInfo.race.registrationPrice,
          vaRegistrationPrice: inscriptionInfo.race.vaRegistrationPrice,
          category: inscriptionInfo.race.category,
          disciplines: inscriptionInfo.race.disciplineDurations.map((dd) => {
            return {
              id: dd.id,
              name: dd.discipline.name,
              duration: dd.duration,
            };
          }),
        },
      },
      files: filesInfo,
    };
  }
}
