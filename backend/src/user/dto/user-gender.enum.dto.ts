export const Gender: {
  MALE: "MALE";
  FEMALE: "FEMALE";
  OTHER: "OTHER";
} = {
  MALE: "MALE",
  FEMALE: "FEMALE",
  OTHER: "OTHER",
};
export type UserGenderDto = (typeof Gender)[keyof typeof Gender];
