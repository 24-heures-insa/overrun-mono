import { InscriptionLiteResponseDto } from "src/inscription/dto/inscription-lite.response.dto";
import { UserGenderDto } from "./user-gender.enum.dto";
export class MyInfoResponseDto {
  id: number;
  email: string;
  username: string;
  currentInscription: InscriptionLiteResponseDto;
  firstName: string;
  lastName: string;
  address: string;
  zipCode: string;
  city: string;
  country: string;
  phoneNumber: string;
  gender: UserGenderDto;
  dateOfBirth: Date;
  files: FileDto[];
}

export class FileDto {
  parentId!: number;
  static edition = class {
    name: string;
    id: number;
  };
  filename!: string;
  url?: string;
  filetype!: FiletypeType;
  static race = class {
    name: string;
    id: number;
  };
}

export const Filetype: {
  CERTIFICATE: "CERTIFICATE";
  PAYMENT: "PAYMENT";
  RESULT: "RESULT";
} = {
  CERTIFICATE: "CERTIFICATE",
  PAYMENT: "PAYMENT",
  RESULT: "RESULT",
};
export type FiletypeType = (typeof Filetype)[keyof typeof Filetype];
