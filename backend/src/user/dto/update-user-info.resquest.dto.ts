import { IsDateString, IsEnum, IsOptional, IsString } from "class-validator";
import { Gender, UserGenderDto } from "./user-gender.enum.dto";

export class UpdateUserInfoDTO {
  @IsString()
  @IsOptional()
  username: string;

  @IsString()
  @IsOptional()
  firstName: string;

  @IsOptional()
  @IsString()
  lastName: string;

  @IsOptional()
  @IsString()
  address: string;

  @IsOptional()
  @IsString()
  zipCode: string;

  @IsOptional()
  @IsString()
  city: string;

  @IsOptional()
  @IsString()
  country: string;

  @IsOptional()
  @IsString()
  phoneNumber: string;

  @IsOptional()
  @IsEnum(Gender)
  gender: UserGenderDto;

  @IsOptional()
  @IsDateString()
  dateOfBirth: Date;
}
