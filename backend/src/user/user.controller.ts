import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiResponse,
  ApiTags,
} from "@nestjs/swagger";
import { Body, Controller, Get, Put, UseGuards } from "@nestjs/common";
import { UserService } from "./user.service";
import { MyInfoResponseDto } from "./dto/my-info.response.dto";
import { UpdateUserInfoDTO } from "./dto/update-user-info.resquest.dto";
import { RolesGuard, ZitadelAuthGuard } from "src/authentication/guards";
import { Roles } from "src/authentication/decorators";
import { oidcRoles } from "@overrun/types";

@UseGuards(ZitadelAuthGuard, RolesGuard)
@ApiTags("users")
@ApiBearerAuth()
@Controller("users")
@ApiBadRequestResponse({
  description: "Bad Request",
})
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get("me")
  @Roles(oidcRoles.ADMIN, oidcRoles.ATHLETE, oidcRoles.TREASURER)
  @ApiResponse({
    status: 200,
    description: "Get information about connected user",
  })
  getMyInfo(): Promise<MyInfoResponseDto> {
    return this.userService.getMyInfo();
  }

  @Put("me")
  @Roles(oidcRoles.ADMIN, oidcRoles.ATHLETE, oidcRoles.TREASURER)
  @ApiBody({
    required: true,
    type: UpdateUserInfoDTO,
  })
  updateUserInfo(@Body() data: UpdateUserInfoDTO) {
    return this.userService.updateMyInfo(data);
  }
}
