import { NestFactory } from "@nestjs/core";
import { SwaggerModule, DocumentBuilder } from "@nestjs/swagger";
import { AppModule } from "./app.module";
import { ValidationPipe } from "@nestjs/common";
import basicAuth from "express-basic-auth";

const SWAGGER_PROTECT_DOMAINS = [
  "overrun.24heures.org",
  "preprod.overrun.24heures.org",
];

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // VALIDATION PIPE FOR CLASS VALIDATION
  app.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true }));
  // CORS
  app.enableCors({
    origin: (origin, callback) => {
      if (!origin || origin.includes(process.env.DOMAIN)) {
        callback(null, true);
      } else {
        callback(new Error("Not allowed by CORS"));
      }
    },
  });
  // SWAGGER
  const config = new DocumentBuilder()
    .setTitle("OverRun")
    .setDescription("The OverRun API description")
    .setVersion(process.env.OVERRUN_VERSION)
    .addBearerAuth()
    .addServer("/rest-api")
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup("swagger", app, document, {
    swaggerOptions: {
      docExpansion: "none",
    },
  });
  // PROTECT SWAGGER
  if (SWAGGER_PROTECT_DOMAINS.includes(process.env.DOMAIN)) {
    app.use(
      "/swagger",
      basicAuth({
        challenge: true,
        users: {
          [process.env.SWAGGER_USER]: process.env.SWAGGER_PASSWORD,
        },
      }),
    );
  }

  await app.listen(3001);
}
bootstrap();
