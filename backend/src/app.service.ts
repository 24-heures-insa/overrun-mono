import { Injectable } from "@nestjs/common";

@Injectable()
export class AppService {
  getHello(): string {
    return `Hello from OverRun backend ${process.env.OVERRUN_VERSION}!`;
  }
}
