## [1.5.0](https://gitlab.com/24-heures-insa/overrun-mono/compare/v1.4.0...v1.5.0) (2025-02-04)

### Features

* **Payment:** ✨ Mise en place du paiement par les athlètes avec Helloasso ([62b9b78](https://gitlab.com/24-heures-insa/overrun-mono/commit/62b9b78aa49c2105e4b20d8472aa34e047da0ff7))
* **VA:** ✨ Correction de l'api VA ([9d5186a](https://gitlab.com/24-heures-insa/overrun-mono/commit/9d5186ae567830eb3eaf5defeef8916ea24848ed))

### Bug Fixes

* **Docker:** correct command for lunchng prod ([85b1b4d](https://gitlab.com/24-heures-insa/overrun-mono/commit/85b1b4d2817f642509237e520d7c6aebd2f075d1))
* **sso:** handle the roles correclty ([7501b39](https://gitlab.com/24-heures-insa/overrun-mono/commit/7501b39e1ecdbd2f30ce3c5e591b0a9080cb666b))

## [1.4.0](https://gitlab.com/24-heures-insa/overrun-mono/compare/v1.3.0...v1.4.0) (2024-11-29)

### Features

* **Backend:** :lock: protect the backend with the user roles ([b2921cd](https://gitlab.com/24-heures-insa/overrun-mono/commit/b2921cd21cd6c0407a9c4d67c86aae64955c8912))
* **Backend:** :tada: add sso connection ([5a1b0d1](https://gitlab.com/24-heures-insa/overrun-mono/commit/5a1b0d181a85d7508301ab0377c12401042add3b))
* **Backend:** ✨ Recuperation des courses dans le back pour les afficher pdt l'inscription ([a9e31da](https://gitlab.com/24-heures-insa/overrun-mono/commit/a9e31da6b762a098c356dff4996f2f427190938e))
* **Backend:** ✨ Validation carte VA avec API EDB ([96e6b8c](https://gitlab.com/24-heures-insa/overrun-mono/commit/96e6b8c00acf95024381acc000cb5d846fa9382c))
* **Front-User:** :construction: Upload et consultation d'un fichier/certificat coureur ([d1fd15c](https://gitlab.com/24-heures-insa/overrun-mono/commit/d1fd15c6ae005b74e779b90175c22f0cf027923a))
* **Front-User:** :sparkles: Inscription à une course ([eee889e](https://gitlab.com/24-heures-insa/overrun-mono/commit/eee889eacc1cc62102dd1da80c9e61c14fac7dc8))
* **Front-User:** ✨ Permettre au coureur de demander l'annulation de leur inscription ([2282a7d](https://gitlab.com/24-heures-insa/overrun-mono/commit/2282a7dd3f3ee3ada11117b7a9c582fff5ef12f3))
* **front:** :camera_flash: put user information in the layout ([c24375d](https://gitlab.com/24-heures-insa/overrun-mono/commit/c24375de75100623592846e9448aa7a76280b05e))
* **front:** :children_crossing: redirect the user by his role ([9229cbe](https://gitlab.com/24-heures-insa/overrun-mono/commit/9229cbe69c1c9c1ef8d59faa6fab740b96823a54))
* **front:** :zap: connect the sso ([0ca64da](https://gitlab.com/24-heures-insa/overrun-mono/commit/0ca64da6566b5a0658d1d7493bd77e1941c54fbd))

### Bug Fixes

* **Docker:** :whale: correct delete .env temp values ([bd57019](https://gitlab.com/24-heures-insa/overrun-mono/commit/bd5701900e3054943218b90e9e67f07482850a09))
* **docker:** typo and ignore docker folder ([14cd41b](https://gitlab.com/24-heures-insa/overrun-mono/commit/14cd41bc530a34d2475f869a9f42f98b0a798a84))
* **front:** :technologist: hot reload is now working ([f044726](https://gitlab.com/24-heures-insa/overrun-mono/commit/f04472668749a09c880609cb9fe94b6510e129ee))

### Refactor

* **Front-Admin:** :art: Option Api vers Composition Api ([9d119af](https://gitlab.com/24-heures-insa/overrun-mono/commit/9d119afe63b7c0863692b29cc0c860c2aa4d2bab))
* **front:** passage de optionAPI vers compositionAPI ([158bb70](https://gitlab.com/24-heures-insa/overrun-mono/commit/158bb70a3800261b2d22c61179a933103f68de68))

## [1.3.0](https://gitlab.com/24-heures-insa/overrun-mono/compare/v1.2.1...v1.3.0) (2024-08-07)

### Features

* **Front-User:** :construction: Page dashboard user ([93b38aa](https://gitlab.com/24-heures-insa/overrun-mono/commit/93b38aa481f5926f2409edc61d2db6c796b60b1b))

## [1.2.1](https://gitlab.com/24-heures-insa/overrun-mono/compare/v1.2.0...v1.2.1) (2024-06-18)

### CI/CD

* **renovate:** 🔧 exclude node lst node release on dockerfiles ([f18d94b](https://gitlab.com/24-heures-insa/overrun-mono/commit/f18d94bcd085bf6dae0462b1c440d764861c3814))
* **renovate:** 🔧 group all node dependencies ([f52d9ee](https://gitlab.com/24-heures-insa/overrun-mono/commit/f52d9ee3ab7d58b37b9312b8009be76ed35caaf5))
* **renovate:** 🔧 include all gitlabci file ([13c8622](https://gitlab.com/24-heures-insa/overrun-mono/commit/13c8622d82d0a4132caf9a33aa136ee435c81147))

## [1.2.0](https://gitlab.com/24-heures-insa/overrun-mono/compare/v1.1.0...v1.2.0) (2024-03-23)


### Features

* **mailing:** add service ([aa7294d](https://gitlab.com/24-heures-insa/overrun-mono/commit/aa7294d4c28a17007e67f5cb38801e00777c91e1))


### CI/CD

* 🏗️ run format on package change ([e2954db](https://gitlab.com/24-heures-insa/overrun-mono/commit/e2954dbc55d69bbc147164e25c9e661dffb939d5))
* **renovate:** 🖊 fix typo ([33955a6](https://gitlab.com/24-heures-insa/overrun-mono/commit/33955a68066e81580fed7129b7bc97b8a369a45a))
* **renovate:** update docker configuration ([07bded6](https://gitlab.com/24-heures-insa/overrun-mono/commit/07bded68b73df82b19d32aa2737da1e0dcd12a90))
* **triage:** remove 0.4 rule ([0bdd649](https://gitlab.com/24-heures-insa/overrun-mono/commit/0bdd64996e41c8ef9549b3a4630f55623c0f2d67))
* update triage rules ([ac9ba2a](https://gitlab.com/24-heures-insa/overrun-mono/commit/ac9ba2a3ede07ac2df27d1d07edea9ba3375c4b2))

## [1.1.0](https://gitlab.com/24-heures-insa/overrun-mono/compare/v1.0.1...v1.1.0) (2023-11-19)


### Features

* **Backend:** :bookmark: display version in backend ([83aca9a](https://gitlab.com/24-heures-insa/overrun-mono/commit/83aca9afab99e38b23c4e4532b4bff4c9ed35ed5))
* **Front-Admin:** :bookmark: display version in frontend admin layout ([a20cb13](https://gitlab.com/24-heures-insa/overrun-mono/commit/a20cb136e99130bfd68409e205d9c17ec5107942))


### Documentation

* Update CHANGELOG.md ([0a94ffa](https://gitlab.com/24-heures-insa/overrun-mono/commit/0a94ffa27b2fc717c6e4d6321f7030b765eacc6a))


### Refactor

* 🚧 move dto definition in packages/types ([4fd8e19](https://gitlab.com/24-heures-insa/overrun-mono/commit/4fd8e19ad6cf76706b5246ce052ac61ae43575ad))

## [1.0.1](https://gitlab.com/24-heures-insa/overrun-mono/compare/v1.0.0...v1.0.1) (2023-11-19)


### CI/CD

* ➕ add conventional-changelog-conventionalcommits ([95e52a2](https://gitlab.com/24-heures-insa/overrun-mono/commit/95e52a2982ad09b99f8f589ebbccdf8ac857b0ca))
* 👷 adjust changelog generation ([191b6ab](https://gitlab.com/24-heures-insa/overrun-mono/commit/191b6ab3b1c1baf8b04b24e9998b02b17e81f339))

# 1.0.0 (2023-11-08)


### Bug Fixes

* :wrench: ts compilation ([e51783b](https://gitlab.com/24-heures-insa/overrun-mono/commit/e51783bb9363c142e4a86ac02928757772d27707))
* **Backend:** :bug: Coherent linter definition between front and back ([6408892](https://gitlab.com/24-heures-insa/overrun-mono/commit/6408892f61bed478997528230f3fa108aa2d5768))
* **deps:** pin dependencies ([5f92746](https://gitlab.com/24-heures-insa/overrun-mono/commit/5f92746e0a837d7b8973e99d042faebe07d69398))
* **deps:** update frontend dependencies ([e4a4bf3](https://gitlab.com/24-heures-insa/overrun-mono/commit/e4a4bf35d4167c3c454d25c28ffac2fe3941d5b1))


### Features

* **Backend:** :card_file_box: add prisma dependency and integration ([f05959f](https://gitlab.com/24-heures-insa/overrun-mono/commit/f05959ff2790a07ddbc1150679babfd451c01e3b))
* **category:** :tada: implementation ([353f8c1](https://gitlab.com/24-heures-insa/overrun-mono/commit/353f8c14c28c613a9cdfbec3d18308b043e978bb))
* **Database:** :card_file_box: Update of database model ([7d7979d](https://gitlab.com/24-heures-insa/overrun-mono/commit/7d7979d82d8f5592be2ea648d083ef73bc3386a4))
* **discipline:** ✨ Add routes, service and front ([cb38587](https://gitlab.com/24-heures-insa/overrun-mono/commit/cb38587066027a32fb6ea0258e963aa26048814c))
* **Docker:** :whale: :tada: dev container ([7499b54](https://gitlab.com/24-heures-insa/overrun-mono/commit/7499b54769564f58d5f817d5e6fcecc12d5e7bfd))
* **Docker:** :whale: dockerize backend ([caeaf27](https://gitlab.com/24-heures-insa/overrun-mono/commit/caeaf27806bfab335bf387dcaaec803ffcdfa00e))
* **Docker:** :whale: dockerize frontend ([07fe136](https://gitlab.com/24-heures-insa/overrun-mono/commit/07fe1367f1adeaf51a47ea9260bdd59630d0120c))
* **edition:** :tada: Implementation ([48e2677](https://gitlab.com/24-heures-insa/overrun-mono/commit/48e26779c2acffe81ad5d7ce72cb1313cde6d331))
* **Front-Admin:** :sparkles: Add race's detaillled page ([8d73ace](https://gitlab.com/24-heures-insa/overrun-mono/commit/8d73ace5918c67b29abd41019a01fbff449a0248))
* **Front-Admin:** :sparkles: Add store and repository for Discipline and Edition ([eb07c75](https://gitlab.com/24-heures-insa/overrun-mono/commit/eb07c7558040c7ae483a8849977e07da420d78bf))
* **Front-Admin:** :sparkles: Creation of Category 's store ([728489e](https://gitlab.com/24-heures-insa/overrun-mono/commit/728489e4c70574f166713c2c56da2895881180c2))
* **renovate:** 🎉 Add renovate.json ([16919c2](https://gitlab.com/24-heures-insa/overrun-mono/commit/16919c20b12c2294f56ed51fc6530b8301e48c4e))
* **triage:** add rules ([c9a6c4f](https://gitlab.com/24-heures-insa/overrun-mono/commit/c9a6c4f150106c0758285e9a389a0b93943649a5))
* **types:** :sparkles: adding types package ([617d0ba](https://gitlab.com/24-heures-insa/overrun-mono/commit/617d0ba6e43567ccc8781551aaa3cb40f6cf405c))
