import unusedImports from "eslint-plugin-unused-imports";
import pluginVue from "eslint-plugin-vue";
import pluginSecurity from "eslint-plugin-security";
import pulginJs from "@eslint/js";
import pluginTs from "typescript-eslint";
import eslintConfigPrettier from "eslint-config-prettier";
import vueParser from "vue-eslint-parser"
import typescriptParser from "@typescript-eslint/parser"

export default pluginTs.config(
  pulginJs.configs.recommended,
  ...pluginTs.configs.recommended,
  pluginSecurity.configs["recommended"],
  eslintConfigPrettier,
  ...pluginVue.configs["flat/recommended"],
  {
    languageOptions: {
      parser: vueParser,
      parserOptions: {
        parser: typescriptParser,
        sourceType: "module"
      }
    }
  },
  {
    rules: {
      "no-undef": "off",
    },
    files: ["**/*.vue"],
  },
  {
    rules: {
      "vue/multi-word-component-names": "off",
    },
    files: ["frontend/pages/**/*.vue"],
  },
  {
    plugins: {
      "unused-imports": unusedImports,
    },
    rules: {
      "unused-imports/no-unused-imports": "error",
      "@typescript-eslint/no-unused-vars": [
        "error",
        {
          args: "all",
          argsIgnorePattern: "^_",
          varsIgnorePattern: "^_",
          caughtErrorsIgnorePattern: "^_",
        },
      ],
    },
  },
  {
    ignores: ["**/build/", "**/dist/", "frontend/.nuxt/", "frontend/.output/"],
  },
);
