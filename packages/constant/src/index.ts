export const ONE_MINUTE_IN_MS = 60 * 1000;
export const SEC_TO_MS = 1000;
export const DAY_TO_MS = 24 * 60 * 60 * 1000;
export const MIN_TO_MS = 60 * 1000;
export const DATE_FORMAT: Intl.DateTimeFormatOptions = {
  year: "numeric",
  month: "2-digit",
  day: "2-digit",
  hour: "2-digit",
  minute: "2-digit",
  timeZone: "Europe/Paris",
};

export const VA_FORMAT = /^c[0-9]{12}$/;

export function getFileExtensionFromFilename(filename: string): FileExtension {
  const ext = filename.split(".").at(-1) ?? "pdf";
  return (
    Object.values(FILE_EXTENSIONS).find(({ extension }) =>
      extension.includes(ext),
    ) ?? FILE_EXTENSIONS[FileExtensionEnum.PDF]
  );
}

export enum FileExtensionEnum {
  PDF,
  IMG_PNG,
  IMG_JPEG,
}

export type FileExtension = {
  extension: string[];
  typeMime: string;
  isImg: boolean;
};

export const FILE_EXTENSIONS: Record<FileExtensionEnum, FileExtension> = {
  [FileExtensionEnum.PDF]: {
    extension: ["pdf"],
    typeMime: "application/pdf",
    isImg: false,
  },
  [FileExtensionEnum.IMG_PNG]: {
    extension: ["png"],
    typeMime: "image/png",
    isImg: true,
  },
  [FileExtensionEnum.IMG_JPEG]: {
    extension: ["jpeg", "jpg"],
    typeMime: "image/jpeg",
    isImg: true,
  },
};
