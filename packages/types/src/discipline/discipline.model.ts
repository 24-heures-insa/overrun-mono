export interface Discipline {
  id?: number;
  name?: string;
  description?: string;
  raceDuration?: DisciplineDuration[];
}

export interface DisciplineDuration {
  id?: number;
  duration?: number;
  race?: {
    name?: string;
    id?: number;
  };
  discipline?: {
    name?: string;
    id?: number;
  };
}
