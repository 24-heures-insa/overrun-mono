export class DisciplineDurationDto {
  id!: number;
  name!: string;
  duration!: number;
}
