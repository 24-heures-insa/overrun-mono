import { Discipline } from "./discipline.model";

export class LiteDisciplineResponse implements Discipline {
  id!: number;

  name!: string;

  description!: string;
}
