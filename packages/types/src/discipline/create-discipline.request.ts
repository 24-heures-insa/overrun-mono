export class CreateDisciplineRequest {
  name!: string;

  description!: string;

  editionId!: number;
}
