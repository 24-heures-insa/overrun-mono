import { Discipline, DisciplineDuration } from "./discipline.model";

export class CompleteDisciplineResponse implements Discipline {
  id!: number;

  name!: string;

  description!: string;

  raceDuration!: DisciplineDuration[];
}
