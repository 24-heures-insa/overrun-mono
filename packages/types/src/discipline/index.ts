export { CompleteDisciplineResponse } from "./complete-discipline.response";
export { LiteDisciplineResponse } from "./lite-discipline.response";
export { CreateDisciplineRequest } from "./create-discipline.request";
export { UpdateDisciplineRequest } from "./update-discipline.request";
export { DisciplineDurationDto } from "./discipline-duration.response";
export type { Discipline, DisciplineDuration } from "./discipline.model";
