export type RawDateString = `${number}-${string}-${string}`;
export type RawDatetimeString =
  `${number}-${string}-${string}T${string}:${string}`;
