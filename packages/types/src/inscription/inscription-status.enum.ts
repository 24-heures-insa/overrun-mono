export const InscriptionStatus: {
  PENDING: "PENDING";
  VALIDATED: "VALIDATED";
  CANCELLED: "CANCELLED";
  CANCEL_ASKED: "CANCEL_ASKED";
} = {
  PENDING: "PENDING",
  VALIDATED: "VALIDATED",
  CANCELLED: "CANCELLED",
  CANCEL_ASKED: "CANCEL_ASKED",
};
export type InscriptionStatus =
  (typeof InscriptionStatus)[keyof typeof InscriptionStatus];
