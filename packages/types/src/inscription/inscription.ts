import { Comment } from "../comment";
import { Certificate } from "../certificate/certificate";
import { Payment } from "../payment/payment";
import { Team } from "../team/team";
import { Race } from "../race/race";
import { User } from "../user/user";
import { VA } from "../vie_asso/va";
import { InscriptionStatus } from "./inscription-status.enum";
import { RawDatetimeString } from "../date";

export interface Inscription {
  id: number;
  status: InscriptionStatus;
  createdAt: Date | RawDatetimeString;
  racingbib: number;
  team?: Team;
  teamAdmin: boolean;
  race: Race;
  comment: Comment;
  certificate?: Certificate;
  va?: VA;
  payment?: Payment;
  athlete: User;
}
