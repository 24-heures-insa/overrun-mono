class JoinTeamDto {
  teamId!: number;
  teamToken!: string;
  isAdmin!: boolean;
}

export class InscriptionCreationDto {
  userId!: number;
  raceId!: number;
  team?: JoinTeamDto;
}
