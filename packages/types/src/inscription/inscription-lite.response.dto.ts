import { CertificateLiteResponseDto } from "../certificate";
import { PaymentLiteResponseDto } from "../payment";
import { RaceLiteResponseDto } from "../race";
import { TeammateResponseDto } from "../team";
import { InscriptionStatus } from "./inscription-status.enum";

export class InscriptionLiteResponseDto {
  id!: number;
  status!: InscriptionStatus;
  racingbib!: number;
  teamMembers?: TeammateResponseDto[];
  teamName?: string;
  isAdmin?: boolean;
  race!: RaceLiteResponseDto;
  va!: boolean;
  certificate!: CertificateLiteResponseDto;
  payment!: PaymentLiteResponseDto;
}
