export class VaDto {
  lastname?: string;
  firstname?: string;
  vaNumber?: string;
  confirmation?: boolean;
}
