import { Inscription } from "../inscription/inscription";

export interface VA {
  id: number;
  va: string;
  inscription: Inscription;
  validated: boolean;
}
