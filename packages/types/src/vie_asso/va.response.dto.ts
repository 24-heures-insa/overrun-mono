import { VAStatus } from "./va-status.enum";

export class VaDtoResponse {
  status!: VAStatus;
  vaMail?: string;
}
