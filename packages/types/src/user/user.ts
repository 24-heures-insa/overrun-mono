import { Certificate } from "../certificate/certificate";
import { Comment } from "../comment";
import { RawDatetimeString } from "../date";
import { Inscription } from "../inscription";
import { UserGenderType, UserRoleType } from "./user.enum";

export interface User {
  id: number;
  email: string;
  username: string;
  password: string;
  role: UserRoleType;
  certificates: Certificate[];
  comments: Comment[];
  inscriptions: Inscription[];
  firstName: string;
  lastName: string;
  address: string;
  zipCode: string;
  city: string;
  country: string;
  phoneNumber: string;
  gender: UserGenderType;
  dateOfBirth: Date | RawDatetimeString;
  resetPasswordToken: string;
}
