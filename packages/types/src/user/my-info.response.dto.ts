import { RawDateString } from "../date";
import { InscriptionLiteResponseDto } from "../inscription";
import { UserGenderType } from "./user.enum";

export class MyInfoResponseDto {
  id!: number;
  email!: string;
  username!: string;
  currentInscription!: InscriptionLiteResponseDto;
  firstName!: string;
  lastName!: string;
  address!: string;
  zipCode!: string;
  city!: string;
  country!: string;
  phoneNumber!: string;
  gender!: UserGenderType;
  dateOfBirth!: Date | RawDateString;
  files!: FileDto[];
}

export class FileDto {
  parentId!: number;
  edition = class {
    name!: string;
    id!: number;
  };
  filename!: string;
  url?: string;
  filetype!: FileCategoryType;
  race = class {
    name!: string;
    id!: number;
  };
}

export const FileCategory: {
  CERTIFICATE: "CERTIFICATE";
  PAYMENT: "PAYMENT";
  RESULT: "RESULT";
} = {
  CERTIFICATE: "CERTIFICATE",
  PAYMENT: "PAYMENT",
  RESULT: "RESULT",
};

export type FileCategoryType = (typeof FileCategory)[keyof typeof FileCategory];
