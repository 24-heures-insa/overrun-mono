export const UserGender: {
  MALE: "MALE";
  FEMALE: "FEMALE";
  OTHER: "OTHER";
} = {
  MALE: "MALE",
  FEMALE: "FEMALE",
  OTHER: "OTHER",
};
export type UserGenderType = (typeof UserGender)[keyof typeof UserGender];

export const UserRole: {
  ATHLETE: "ATHLETE";
  TREASURER: "TREASURER";
  ADMIN: "ADMIN";
  OLD_ADMIN: "OLD_ADMIN";
} = {
  ATHLETE: "ATHLETE",
  TREASURER: "TREASURER",
  ADMIN: "ADMIN",
  OLD_ADMIN: "OLD_ADMIN",
};
export type UserRoleType = (typeof UserRole)[keyof typeof UserRole];
