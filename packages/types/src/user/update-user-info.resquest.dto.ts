import { RawDateString } from "../date";
import { UserGenderType } from "./user.enum";

export class UpdateUserInfoDTO {
  username?: string;
  firstName?: string;
  lastName?: string;
  address?: string;
  zipCode?: string;
  city?: string;
  country?: string;
  phoneNumber?: string;
  gender?: UserGenderType;
  dateOfBirth?: Date | RawDateString;
}
