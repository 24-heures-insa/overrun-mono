export enum oidcRoles {
  ADMIN = "overrun_admin",
  ATHLETE = "overrun_athlete",
  TREASURER = "overrun_treasurer",
}

export type oidcRolesType = (typeof oidcRoles)[keyof typeof oidcRoles];
