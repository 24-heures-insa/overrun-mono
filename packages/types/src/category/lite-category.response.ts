import { Category } from "./category.model";

export class LiteCategoryResponse implements Category {
  id!: number;

  name!: string;

  description!: string;

  maxTeamMembers!: number;

  minTeamMembers!: number;
}
