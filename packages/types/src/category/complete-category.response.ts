import { Category } from "./category.model";

export class CompleteCategoryResponse implements Category {
  id!: number;

  name!: string;

  description!: string;

  maxTeamMembers!: number;

  minTeamMembers!: number;

  // races: Race[];
}
