import { Race } from "../race/race";
import { Edition } from "../edition/edition.model";

export interface Category {
  id: number;
  name: string;
  description?: string;
  maxTeamMembers: number;
  minTeamMembers: number;
  races?: Race[];
  edition?: Edition;
}
