export class CreateCategoryRequest {
  name!: string;

  description!: string;

  editionId!: number;

  maxTeamMembers!: number;

  minTeamMembers!: number;
}
