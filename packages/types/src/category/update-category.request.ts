export class UpdateCategoryRequest {
  name?: string;
  description?: string;
  maxTeamMembers?: number;
  minTeamMembers?: number;
}
