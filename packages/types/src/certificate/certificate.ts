import { RawDatetimeString } from "../date";
import { Inscription } from "../inscription";
import { User } from "../user/user";
import { CertificateStatusType } from "./certificate-status.enum";

export interface Certificate {
  id: number;
  filename: string;
  inscription: Inscription;
  inscriptionId: number;
  uploadedAt: Date | RawDatetimeString;
  status: CertificateStatusType;
  statusUpdatedAt?: Date | RawDatetimeString;
  statusUpdatedBy?: User;
  statusUpdatedById?: number;
}
