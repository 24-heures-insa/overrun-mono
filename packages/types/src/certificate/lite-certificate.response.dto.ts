import { CertificateStatusType } from "./certificate-status.enum";

export class CertificateLiteResponseDto {
  id!: number;
  filename!: string;
  status!: CertificateStatusType;
}
