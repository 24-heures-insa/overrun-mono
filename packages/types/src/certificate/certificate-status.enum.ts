export enum CertificateStatus {
  VALIDATED = "VALIDATED",
  PENDING = "PENDING",
  REFUSED = "REFUSED",
}

export type CertificateStatusType =
  (typeof CertificateStatus)[keyof typeof CertificateStatus];
