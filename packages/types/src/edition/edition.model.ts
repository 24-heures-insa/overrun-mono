import { RawDatetimeString } from "../date";

export interface Edition {
  id: number;
  name: string;
  startDate: Date | RawDatetimeString;
  endDate: Date | RawDatetimeString;
  registrationStartDate: Date | RawDatetimeString;
  registrationEndDate: Date | RawDatetimeString;
  active: boolean;
}
