import { RawDatetimeString } from "../date";

export class UpdateEditionRequest {
  name?: string;

  startDate?: Date | RawDatetimeString;

  endDate?: Date | RawDatetimeString;

  registrationStartDate?: Date | RawDatetimeString;

  registrationEndDate?: Date | RawDatetimeString;
}
