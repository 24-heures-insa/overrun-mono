import { RawDatetimeString } from "../date";
import { Edition } from "./edition.model";

export class CompleteEditionResponse implements Edition {
  id!: number;

  name!: string;

  active!: boolean;

  registrationStartDate!: Date | RawDatetimeString;

  registrationEndDate!: Date | RawDatetimeString;

  startDate!: Date | RawDatetimeString;

  endDate!: Date | RawDatetimeString;

  fundraiser?: { description: string };
}
