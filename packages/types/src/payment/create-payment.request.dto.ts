export class CreatePaymentDto {
  inscriptionId!: number;
  raceAmount!: number;
  donationAmount?: number;
  paymentLabel!: string;
}
