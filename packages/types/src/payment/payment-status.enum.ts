export const PaymentStatus: {
  NOT_STARTED: "NOT_STARTED";
  PENDING: "PENDING";
  VALIDATED: "VALIDATED";
  REFUSED: "REFUSED";
  REFUND: "REFUND";
  REFUNDING: "REFUNDING";
} = {
  NOT_STARTED: "NOT_STARTED",
  PENDING: "PENDING",
  VALIDATED: "VALIDATED",
  REFUSED: "REFUSED",
  REFUND: "REFUND",
  REFUNDING: "REFUNDING",
};

export type PaymentStatus = (typeof PaymentStatus)[keyof typeof PaymentStatus];
