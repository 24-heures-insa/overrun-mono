import { RawDatetimeString } from "../date";
import { PaymentStatus } from "./payment-status.enum";

export class PaymentLiteResponseDto {
  id!: number;
  raceAmount!: number;
  donationAmount!: number;
  totalAmount!: number;
  status!: PaymentStatus;
  refundAsked!: boolean;
  helloassoCheckoutExpiresAt?: RawDatetimeString;
  helloassoCheckoutIntentUrl?: string;
  helloassoPaymentReceiptUrl?: string;
}
