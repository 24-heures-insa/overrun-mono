import { RawDatetimeString } from "../date";
import { Inscription } from "../inscription";
import { PaymentStatus } from "./payment-status.enum";

export interface Payment {
  id: number;
  inscription: Inscription;
  inscriptionId: number;
  date: Date | RawDatetimeString;
  raceAmount: number;
  donationAmount: number;
  totalAmount: number;
  status: PaymentStatus;
  helloassoCheckoutIntentId?: number;
  helloassoCheckoutIntentUrl?: string;
  helloassoCheckoutExpiresAt: Date | RawDatetimeString;
  helloassoPaymentReceiptUrl: string;
  refundAsked: boolean;
}
