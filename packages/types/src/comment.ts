import { User } from "./user/user";
import { Inscription } from "./inscription";
import { RawDatetimeString } from "./date";

export interface Comment {
  id: number;
  comment: string;
  date: Date | RawDatetimeString;
  inscription: Inscription;
  author: User;
}
