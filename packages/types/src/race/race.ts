import { Category } from "../category";
import { DisciplineDuration } from "../discipline";
import { Inscription } from "../inscription";
import { Team } from "../team/team";

export interface Race {
  id: number;
  name: string;
  registrationPrice?: number;
  vaRegistrationPrice?: number;
  maxTeams?: number;
  disciplineDurations?: DisciplineDuration[];
  category?: Category;
  inscriptions?: Inscription[];
  teams?: Team[];
}
