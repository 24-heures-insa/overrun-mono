import { DisciplineDurationDto } from "../discipline";
import { LiteCategoryResponse } from "../category";

export class RaceLiteResponseDto {
  id!: number;
  name!: string;
  registrationPrice?: number;
  vaRegistrationPrice?: number;
  disciplines!: DisciplineDurationDto[];
  category?: LiteCategoryResponse;
}
