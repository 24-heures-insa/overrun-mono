export class TeamCreatedDto {
  idTeam!: number;
  name!: string;
  token!: string;
}
