export class CompleteTeamDto {
  id!: number;
  name!: string;
  freeSpots!: number;
  totalSpots!: number;
  raceId!: number;
  categoryId!: number;
  disciplineIds!: number[];
}
