import { Inscription } from "../inscription";
import { Race } from "../race/race";

export interface Team {
  id: number;
  name: string;
  password: string;
  members: Inscription[];
  race: Race;
  admins: Inscription[];
}
