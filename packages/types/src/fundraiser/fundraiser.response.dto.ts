export class FundraiserDto {
  id!: number;
  name!: string;
  description!: string;
  editionId!: number;
}
