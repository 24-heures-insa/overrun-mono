import { Edition } from "../edition/edition.model";

export interface Fundraiser {
  id: number;
  name: string;
  description: string;
  edition: Edition;
}
