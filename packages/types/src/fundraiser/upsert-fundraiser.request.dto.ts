export class UpsertFundraiserDto {
  name?: string;
  description?: string;
  images?: string[];
  editionId!: number;
}
